
import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { Fonts, Colors, Icons, SafeArea } from '../GlobalConfig';
import CustomButton from '../components/CustomButton';
import Modal from 'react-native-modal';

class OptionItem extends PureComponent {
    render() {
        const { item, selected, changeSelected, disabled } = this.props
        return (
            <TouchableOpacity
                disabled={disabled}
                onPress={() => changeSelected(item)}
                style={{ height: 30, width: '100%', marginTop: 12, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{
                        fontSize: 14,
                        // fontFamily: Fonts.Nunito,
                        lineHeight: 24,
                        letterSpacing: 0.5,
                        color: Colors.GreyColor
                    }}>{item}</Text>
                </View>
                <View style={{ width: 20, height: 20, borderRadius: 10, borderWidth: 2, borderColor: selected ? Colors.PrimaryColor : Colors.SemiGreyColor, padding: 3 }}>
                    {selected && <View style={{ flex: 1, borderRadius: 7, backgroundColor: Colors.PrimaryColor }} />}
                </View>
            </TouchableOpacity>
        )
    }
}

export default class CustomCancelModal extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selected: '',
            customReason: ''
        }
    }

    changeSelected = (item) => {
        this.setState({
            selected: item
        })
    }

    submit = () => {
        this.props.onSubmitCancelForm(this.state.selected)
    }

    render() {
        const { selected } = this.state
        const {
            modalText = `Do you want to\ncancel this order?`,
            cancelOptionList = [],
            allowOption = true,
            onCancelPress,
            cancelModalVisible,
            isLoadingCancel
        } = this.props

        const modalContent = (
            <View style={[styles.modalBox, allowOption && { height: 440 }]}>
                <TouchableOpacity
                    disabled={isLoadingCancel}
                    onPress={onCancelPress}
                    style={styles.closeContainer}>
                    <Image source={Icons.iconDelete} style={{ flex: 1 }} resizeMode='contain' />
                </TouchableOpacity>
                <View style={{ flex: 1, paddingTop: 50 }}>
                    <Text style={styles.modalText}>{modalText}</Text>
                    <View style={{ borderTopWidth: 1, borderTopColor: Colors.SemiGreyColor, marginHorizontal: 30 }} />
                    <View style={{ flex: 1, marginBottom: 20 }}>
                        <ScrollView keyboardShouldPersistTaps='handled' contentContainerStyle={{ paddingHorizontal: 30, paddingBottom: 20 }}>
                            {allowOption && (
                                <>
                                    {cancelOptionList.map((item, index) => (
                                        <OptionItem
                                            item={item}
                                            index={index}
                                            key={index}
                                            disabled={isLoadingCancel}
                                            changeSelected={this.changeSelected}
                                            selected={selected == item} />
                                    ))}
                                </>
                            )}
                        </ScrollView>
                    </View>
                    <View style={{ height: 40, width: '100%', flexDirection: 'row', marginBottom: 20, paddingHorizontal: 30 }}>
                        <View style={{ flex: 1, marginRight: SafeArea }}>
                            <CustomButton
                                outline
                                isLoading={isLoadingCancel}
                                onPress={onCancelPress}
                                label='No'
                            />
                        </View>
                        <View style={{ flex: 1, marginLeft: SafeArea }}>
                            <CustomButton
                                disabled={!selected}
                                isLoading={isLoadingCancel}
                                onPress={this.submit}
                                label='Yes'
                            />
                        </View>
                    </View>
                </View>
            </View>
        )

        return (
            <Modal
                style={{ margin: 0, justifyContent: 'flex-end' }}
                children={modalContent}
                isVisible={cancelModalVisible}
                backdropOpacity={0.7}
            />
        )
    }
}

const styles = StyleSheet.create({
    modalBox: {
        height: 220,
        width: '100%',
        backgroundColor: Colors.WhiteColor,
        paddingVertical: 12
    },
    closeContainer: {
        height: 25,
        width: 25,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 25,
        left: 24,
        zIndex: 2
    },
    modalText: {
        fontSize: 18,
        // fontFamily: Fonts.NunitoBold,
        paddingHorizontal: 30,
        lineHeight: 28,
        letterSpacing: 0.5,
        marginBottom: 20
    }
})