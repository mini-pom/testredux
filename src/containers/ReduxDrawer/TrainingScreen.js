import React, { Component, PureComponent } from "react";
import { View, Text, Image } from "react-native";
import SwiperFlatList from 'react-native-swiper-flatlist';

class BannerItem extends PureComponent {
	render() {
		const { name, image } = this.props.item
		return (
			<View style={{ height: 180, width: 300, padding: 20 }}>
				<View style={{ flex: 1 }}>
					<Image style={{ flex: 1 }} source={{ uri: image }} />
				</View>
				<View style={{ flex: 1, backgroundColor: 'yellow' }}>
					<Text>{name}</Text>
				</View>
			</View>
		)
	}
}

class Halaman extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [
				{
					name: 'Gambar 1',
					image: 'https://cdn.pixabay.com/photo/2015/11/02/18/34/banner-1018818_960_720.jpg'
				},
				{
					name: 'Gambar 2',
					image: 'https://cdn.pixabay.com/photo/2015/09/26/11/21/banner-958962_960_720.jpg'
				},
				{
					name: 'Gambar 3',
					image: 'https://cdn.pixabay.com/photo/2015/11/02/18/34/banner-1018818_960_720.jpg'
				},
				{
					name: 'Gambar 4',
					image: 'https://cdn.pixabay.com/photo/2015/09/26/11/21/banner-958962_960_720.jpg'
				},
				{
					name: 'Gambar 5',
					image: 'https://cdn.pixabay.com/photo/2015/11/02/18/34/banner-1018818_960_720.jpg'
				},
				{
					name: 'Gambar 6',
					image: 'https://cdn.pixabay.com/photo/2015/09/26/11/21/banner-958962_960_720.jpg'
				},
			],
			activeSlide: 0
		}
	}

	render() {
		const { data } = this.state
		return (
			<View style={{ flex: 1, backgroundColor: 'white' }}>
				<View style={{ height: 250, width: '100%', backgroundColor: 'blue' }}>
					<SwiperFlatList
						data={data}
						extraData={data}
						// autoplayDelay={2}
						// autoplayLoop
						showPagination
						renderItem={({ item, index }) => <BannerItem item={item} index={index} />}
						PaginationComponent={({ paginationIndex }) => (
							<View style={{ height: 50, backgroundColor: 'red', flexDirection: 'row', alignItems: 'center' }}>
								{data.length && data.map((item, index) => (
									<View key={`page_${index}`} style={[{ height: 20, aspectRatio: 1, backgroundColor: 'green', marginRight: 20 }, index == paginationIndex && { backgroundColor: 'blue' }]} />
								))}
							</View>
						)}
					/>
				</View>
			</View>
		);
	}
}

export default Halaman;