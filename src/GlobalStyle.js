import { StyleSheet } from 'react-native'

const GlobalStyle = StyleSheet.create({
	container: {
		justifyContent: "space-around",
		alignItems: "center",
		flex: 1
	},
	customBtnText: {
		fontSize: 24,
		fontWeight: "400",
		color: "#fff",
		textAlign: "center"
	},
	customBtnBG: {
		backgroundColor: "#007aff",
		paddingHorizontal: 10,
		paddingVertical: 5,
		borderRadius: 10,
		minWidth: 50
	},
	counter: {
		fontSize: 40,
		fontWeight: "400",
		color: "#007aff",
		minWidth: 100,
		textAlign: "center"
	},
})

export default GlobalStyle;