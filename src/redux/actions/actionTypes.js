import ACTION_TYPES from '.';

export const incrementBy = by => {
    return {
        type: ACTION_TYPES.INCREMENT,
        payload: by
    };
};

export const decrementBy = by => {
    return {
        type: ACTION_TYPES.DECREMENT,
        payload: by
    };
};

export const clear = ({
    type: ACTION_TYPES.CLEAR
})