
import React, { PureComponent } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TouchableNativeFeedback,
    ActivityIndicator,
    ViewPropTypes
} from 'react-native';
import { Fonts, Colors, Icons, SafeArea } from '../GlobalConfig';
import PropTypes from 'prop-types';

export default class CustomButton extends PureComponent {
	static propTypes = {
		containerStyle: ViewPropTypes.style,
		style: ViewPropTypes.style,
		label: PropTypes.string,
		onPress: PropTypes.func,
	}

	render() {
		const { label, onPress, customColor, outline, ...props } = this.props;
		const buttonStyle = {
			alignItems: 'center',
			justifyContent: 'center',
			height: 40,
			width: '100%',
			borderRadius: 4
		}
		const buttonShadowStyle = {
			backgroundColor: customColor ? customColor : outline ? Colors.WhiteColor : Colors.PrimaryColor,
			// shadowColor: "#000",
			// shadowOffset: {
			// 	width: 0,
			// 	height: 1,
			// },
			// shadowOpacity: 0.22,
			// shadowRadius: 2.22,
			// elevation: 3,
		}
		const buttonDisabledShadowStyle = {
			backgroundColor: Colors.GreyColor,
			// shadowColor: "#000",
			// shadowOffset: {
			// 	width: 0,
			// 	height: 1,
			// },
			// shadowOpacity: 0.20,
			// shadowRadius: 1.41,

			// elevation: 1,
		}
		const buttonText = {
			color: outline ? Colors.PrimaryColor : Colors.WhiteColor,
			// fontFamily: WorkSansSemiBold,
			letterSpacing: 0.5
		}
		if (Platform.OS == 'android') {
			return (
				<TouchableNativeFeedback disabled={props.isLoading} onPress={onPress} {...props}>
					<View style={[
						buttonStyle,
						outline && { borderWidth: 1, borderColor: Colors.PrimaryColor },
						props.disabled || props.isLoading ? buttonDisabledShadowStyle : buttonShadowStyle
					]}>
						{props.isLoading ?
							<ActivityIndicator color={Colors.WhiteColor} size='small' /> :
							<Text style={buttonText}>{label}</Text>}
					</View>
				</TouchableNativeFeedback>
			)
		}
		return (
			<TouchableOpacity
				style={[buttonStyle, props.disabled || props.isLoading ? buttonDisabledShadowStyle : buttonShadowStyle]} disabled={props.isLoading} onPress={onPress} {...props}>
				{props.isLoading ?
					<ActivityIndicator color={Colors.WhiteColor} size='small' /> :
					<Text style={buttonText}>{label}</Text>}
			</TouchableOpacity>
		)
	}
}