import React, { Component, PureComponent } from "react";
import { TouchableNativeFeedback, TouchableOpacity, View, Text, Easing, Platform } from 'react-native'
import { Router, Stack, Scene, Tabs, Actions, Drawer } from "react-native-router-flux";
import { connect } from "react-redux";
import TestReduxScreen from "../containers/ReduxTab/TestReduxScreen";
import ReduxResultScreen from "../containers/ReduxTab/ReduxResultScreen";
import MenuScreen from "../containers/MenuScreen";
import LinkedInScreen from "../containers/LinkedInScreen";
import PushNotificationScreen from "../containers/PushNotificationScreen";
import TestSoundScreen from "../containers/TestSoundScreen";
import WidgetScreen from "../containers/WidgetScreen";
import XenditScreen from "../containers/Xendit/XenditScreen";
import TokenScreen from "../containers/Xendit/TokenScreen";
import AuthenticationScreen from "../containers/Xendit/AuthenticationScreen";
import LinkedSelectionPickerScreen from "../containers/LinkedSelectionPickerScreen";
import FaceRecognitionScreen from "../containers/FaceRecognitionScreen";
import DemoMapScreen from "../containers/MapScreen";
import TrainingScreen from "../containers/ReduxDrawer/TrainingScreen";
import TrainingScreen2 from "../containers/ReduxDrawer/TrainingScreen2";
import DrawerContent from "../components/DrawerContent";
import CustomRightIndicator from "../components/CustomRightIndicator";

var focusedColor = '#cc8899'
var inactiveColor = '#aaa'

const MyTransitionSpec = ({
	duration: 1000,
	easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
	// timing: Animated.timing,
});

const transitionConfig = () => ({
	transitionSpec: MyTransitionSpec,
	screenInterpolator: sceneProps => {
		const { layout, position, scene } = sceneProps;
		const { index } = scene;
		const width = layout.initWidth;
		const height = layout.initHeight;

		// right to left by replacing bottom scene
		return {
			transform: [{
				translateX: position.interpolate({
					inputRange: [index - 1, index, index + 1],
					outputRange: [width, 0, -width],
				}),
				// translateY: position.interpolate({
				// 	inputRange: [index - 1, index, index + 1],
				// 	outputRange: [height, 0, -height],
				// }),
			}]
		};
	}
});

class TabIcon extends PureComponent {
	navigate(pathName) {
		// if (pathName === 'home' || pathName === 'industryCategory') {
		// }
		Actions.jump(pathName)
	}
	render() {
		const screenProps = {
			...this.props.iconProps,
			counter: this.props.counter
		}
		var iconName, pathName
		if (this.props.iconProps.title === 'Add Data') {
			pathName = 'testRedux'
			iconName = 'home'
		}
		else if (this.props.iconProps.title === 'Result Data') {
			pathName = 'resultRedux'
			iconName = 'grid'
		}

		if (Platform.OS === 'android') {
			return (
				<TouchableNativeFeedback
					onPress={() => this.navigate(pathName)}
					background={Platform.Version >= 21 ?
						TouchableNativeFeedback.Ripple('blue', true) :
						TouchableNativeFeedback.SelectableBackground()}>
					<View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 1 }}>
						<View style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center' }}>
							{/* <Feather
								size={this.screenProps.focused ? 24 : 20}
								color={this.screenProps.focused ? focusedColor : inactiveColor}
								name={iconName}
							/> */}
							{/* {this.screenProps.cart_quantity &&
                                <View style={{ position: 'absolute', top: 0, right: -5, width: 15, height: 15, backgroundColor: BlueColor, borderRadius: 8, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: WorkSansSemiBold, fontSize: 10, color: WhiteColor }}>{this.screenProps.cart_quantity}</Text>
                                </View>} */}
							<Text>{screenProps.counter}</Text>
						</View>
						<Text style={[{ fontSize: 10, marginTop: 5 }, screenProps.focused ? { color: focusedColor } : { color: inactiveColor }]}>{screenProps.title}</Text>
					</View>
				</TouchableNativeFeedback>
			)
		}
		else {
			return (
				<TouchableOpacity onPress={() => this.navigate(pathName)}>
					<View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', zIndex: 1 }}>
						<View style={{ height: 25, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
							{/* <Feather
								size={this.screenProps.focused ? 24 : 20}
								color={this.screenProps.focused ? focusedColor : inactiveColor}
								name={iconName}
							/> */}
							<Text>{screenProps.counter}</Text>
						</View>
						<Text style={[{ fontSize: 10 }, screenProps.focused ? { color: focusedColor } : { color: inactiveColor }]}>{screenProps.title}</Text>
					</View>
				</TouchableOpacity>
			);
		}
	}
}

class NavigationRouter extends Component {
	shouldComponentUpdate() { return false }
	render() {
		return (
			<Router>
				<Stack
					// navigationBarStyle={{ backgroundColor: '#000' }}
					transitionConfig={transitionConfig}
					key='root'>
					<Scene key='menu'
						initial
						title='Menu'
						renderRightButton={() => <CustomRightIndicator />}
						component={MenuScreen} />
					<Scene key='linkedInTest'
						back
						title='LinkedIn Integration'
						component={LinkedInScreen} />
					<Scene key='pushNotification'
						back
						title='Push Notification'
						component={PushNotificationScreen} />
					<Scene key='xendit'
						back
						title='Xendit'
						component={XenditScreen} />
					<Scene key='xenditToken'
						back
						title='Test Tokenization'
						component={TokenScreen} />
					<Scene key='xenditAuth'
						back
						title='Test 3DS'
						component={AuthenticationScreen} />
					<Scene key='widget'
						back
						title='Setup Widget'
						renderRightButton={() => <CustomRightIndicator />}
						component={WidgetScreen} />
					<Scene key='testSound'
						back
						title='Test Sound'
						renderRightButton={() => <CustomRightIndicator />}
						component={TestSoundScreen} />
					<Scene key='linkedSelection'
						back
						title='Linked Selection'
						component={LinkedSelectionPickerScreen} />
					<Scene key='faceRecognition'
						back
						title='Face Recognition'
						component={FaceRecognitionScreen} />
					<Scene key='demoMap'
						back
						hideNavBar
						title='Map Implementation'
						component={DemoMapScreen} />
					{/* <Tabs
						key='tab'
						tabBarStyle={{ backgroundColor: '#fff', height: 60 }}
						showLabel={false}
						hideNavBar>
						<Scene
							key='testRedux'
							initial
							title='Add Data'
							icon={(iconProps) => <TabIcon iconProps={iconProps} counter={this.props.counter} />}
							// hideNavBar
							component={TestReduxScreen} />
						<Scene
							key='resultRedux'
							title='Result Data'
							icon={(iconProps) => <TabIcon iconProps={iconProps} counter={this.props.counter} />}
							// hideNavBar
							component={ReduxResultScreen} />
					</Tabs> */}
					<Drawer
						// onEnter={() => Actions.refresh({ lastUpdateProfile: new Date })}
						drawerIcon={<Text>Drawer</Text>}
						hideNavBar
						key="drawerMenu"
						contentComponent={DrawerContent}
						drawerWidth={300}
						drawerLockMode='locked-closed'>
						<Stack
							transitionConfig={transitionConfig}
							// navigationBarStyle={Platform.OS == 'ios' && { height: 55 }}
							key='drawerRoot'>
							<Scene
								key='testRedux'
								initial
								title='Add Data'
								icon={(iconProps) => <TabIcon iconProps={iconProps} counter={this.props.counter} />}
								// hideNavBar
								component={TestReduxScreen} />
							<Scene
								key='resultRedux'
								title='Result Data'
								icon={(iconProps) => <TabIcon iconProps={iconProps} counter={this.props.counter} />}
								// hideNavBar
								component={ReduxResultScreen} />
							{/* <Scene key='training'
								drawer
								title='Training'
								component={TrainingScreen} />
							<Scene key='training2'
								back
								title='Detail'
								component={TrainingScreen2} /> */}
						</Stack>
					</Drawer>
				</Stack>
			</Router>
		);
	}
}

// const mapStateToProps = state => {
// 	return {
// 		counter: state.counterOperation.counter
// 	};
// };

// export default connect(mapStateToProps)(NavigationRouter);
export default NavigationRouter