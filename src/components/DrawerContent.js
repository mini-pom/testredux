import React, { Component, PureComponent } from "react";
import { View, Text, Image } from "react-native";
import { APIGetEmployeeDetail } from "../APIConfig";
import { Colors } from "../GlobalConfig";
import { connect } from "react-redux";
import { clear, decrementBy } from "../redux/actions/actionTypes";

class DrawerContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nama: '',
		}
	}

	// componentDidUpdate = (prevProps, prevState) => {
	// 	if (prevProps.counter != this.props.counter) {
	// 		if (this.props.counter == 5) this.props.remove(4)
	// 	}
	// };


	componentDidMount() {
	}

	render() {
		const { counter } = this.props
		return (
			<View style={{ flex: 1 }}>
				<Text style={{ fontSize: 48 }}>
					{counter}
				</Text>
			</View>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		remove: by => {
			dispatch(decrementBy(by));
		},
	};
};

const mapStateToProps = state => {
	return {
		counter: state.counterOperation.counter
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);