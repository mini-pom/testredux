import { Platform, Linking } from "react-native";

/**
 * Run the function after x ms
 * @param {milisec} ms 
 */
export function wait(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

/**
 * Check if String is null return true if not empty
 * @param {String} condition 
 */
export function isNull(condition) {
    return condition !== null ? true : false
}

/**
 * Check if String is JSON
 * @param {String} condition 
 */
export function isJSON(str) {
    try {
        return (JSON.parse(str) && !!str);
    } catch (e) {
        return false;
    }
}

/**
 * Check if String is over the max lenght return true if over
 * @param {String} condition 
 * @param {Int} max 
 */
export function isLength(param, max) {
    return param !== null && param.length >= max ? true : false
}

/**
 * Check the formatting of email
 * @param {String} param 
 */
export function isNotEmail(param) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    return reg.test(param) === false ? true : false
}

/**
 * Convert Int or String to IDR format
 * @param {Int} value 
 */
export const idr = (value) => {
    if (value) return "Rp " + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    else return 'Rp 0'
}

/**
 * Convert Int or String to IDR format
 * @param {Int} value 
 */
export const idrNoRp = (value) => {
    if (value) return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    else return '0'
}

/**
 * Open The Phone Default Dial Pad
 * Autofill The Number with Received Params
 */
export function call(number = "") {
    var url = `${Platform.OS === 'ios' ? 'telprompt:' : 'tel:'}${number}`
    Linking.canOpenURL(url).then(canOpen => {
        if (canOpen) {
            return Linking.openURL(url).catch((err) => Promise.reject(err))
        }
    })
}

/**
 * Open The Phone Default SMS Screen
 * Autofill The Number with Received Params
 */
export function sms(number = "", body = '') {
    var url = `sms:${number}${Platform.OS === "ios" ? "&" : "?"}body=${body}`
    Linking.canOpenURL(url).then(canOpen => {
        if (canOpen) {
            return Linking.openURL(url).catch((err) => Promise.reject(err))
        }
    })
}

/**
 * Open The Phone Default Email
 * Autofill The To and Subject
 */
export function email(to = "cs@profitto.com", subject = "[Profitto] - Question") {
    Linking.openURL(`mailto:${to}?subject=${subject}`)
}

/**
 * Open The Play Store or App Store
 */
export function openStore() {
    var packageName = Platform.OS == 'android' ? 'com.jasabox' : 'org.react.native.jasabox.v2'
    Linking.openURL(`market://details?id=${packageName}`);
}