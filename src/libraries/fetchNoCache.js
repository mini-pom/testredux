export default async (update, API, requestType = 'GET', tokenType = '', post_data = null, params_data = null, debugMode = false) => {
    let token

    return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
        method: requestType,
        headers: post_data ?
            {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            } :
            {
                'Authorization': `Bearer ${token}`
            },
        body: post_data ? JSON.stringify(post_data) : ''
    })
        .then((response) => {
            if (debugMode) {
                console.log({
                    API: `${API}${params_data ? `?${params_data}` : ``}`,
                    post_data: post_data
                })
                console.log(response.text())
                console.log(response.status)
            }
            const statusCode = response.status;
            const res = response.json()
            return Promise.all([statusCode, res])
        })
        .then(([statusCode, result]) => {
            update(
                {
                    "status": statusCode,
                    "result": result,
                    "isCache": false
                }
            )
        })
        .catch(err => { throw err })

}