export const LinkedInClientID = '816g9uco299ue9'

export const LinkedInClientSecret = 'wKugmtoze4JYEtqR'

export const LinkedInRedirectURL = 'www.google.com'

export const XenditDevelopmentKey = 'xnd_public_development_PKyr2aQlHH7yrZw354dshroYoKuhy2J8XZeKVfnbgD8DCpvdbU0yzcaigRw3DX9'

export const convertPostMessageScript = `(function() {
	window.postMessage = function(data) {
	  window.ReactNativeWebView.postMessage(data);
	};
})()`

export const SafeArea = 16

// export const Colors = {
// 	Green : 'green'
// }

export const Colors = Object.freeze({
	Green: 'green',
	PrimaryColor: '#5dbb2c',
	WhiteColor: '#fff',
	// BlackColor: '#000',
	GreyColor: 'grey',
	SemiGreyColor: '#e2e2e2',
	LightGreyColor: '#fbfbfb',
	// PurpleColor: '#eef',
	// BlackFontColor: '#1c1c1c',
})

export const Icons = Object.freeze({
	iconDelete: require('./assets/images/icons/ic_delete_2.png'),
	iconMapLocationMe: require('./assets/images/icons/ic_map_location_me.png'),
	iconMapMotorcycle: require('./assets/images/icons/ic_map_motorcycle.png'),
	iconMapCar: require('./assets/images/icons/ic_map_car.png'),
	iconMapPickUp: require('./assets/images/icons/ic_map_pickup.png'),
	iconMapPointerDestination: require('./assets/images/icons/ic_map_pointer_destination.png'),
	iconGoogleLogo: require('./assets/images/icons/google_logo.png'),
	iconMap: require('./assets/images/icons/ic_map.png'),
	iconLocation2: require('./assets/images/icons/ic_location_2.png'),
	iconLeftArrow: require('./assets/images/icons/ic_left_arrow.png'),
	iconPickLocation: require('./assets/images/icons/ic_pick_location.png'),
	iconMotorcycle: require('./assets/images/icons/ic_motorcycle.png'),
	iconCar: require('./assets/images/icons/ic_car.png'),
	iconPickUp: require('./assets/images/icons/ic_pickup.png'),
	iconDestinations: require('./assets/images/icons/ic_destinations.png'),
	iconWallet: require('./assets/images/icons/ic_wallet.png'),
	iconArrow2: require('./assets/images/icons/ic_right_arrow.png'),
	iconApprove2: require('./assets/images/icons/ic_approve_2.png'),
	iconNotif: require('./assets/images/icons/ic_notif.png'),
	iconPhoneColor: require('./assets/images/icons/ic_phone_color.png'),
	iconChat: require('./assets/images/icons/ic_chat.png'),
	iconCash: require('./assets/images/icons/ic_cash.png'),
})