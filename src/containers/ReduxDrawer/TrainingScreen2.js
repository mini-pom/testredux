import React, { Component, PureComponent } from "react";
import { View, Text, Image } from "react-native";
import { APIGetEmployeeDetail } from "../../APIConfig";
import { Colors } from "../../GlobalConfig";

class Halaman extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nama: '',
		}
	}

	getData() {
		const { id } = this.props
		// const id = '2'
		fetch(`${APIGetEmployeeDetail}${id}`)
			.then(res => res.json())
			.then(resJson => {
				console.log(resJson)
				this.setState({
					// data: JSON.stringify(resJson)
					nama: resJson.data.employee_name
				})
			})
	}

	componentDidMount() {
		this.getData()
	}

	render() {
		const { nama } = this.state
		const { id } = this.props
		return (
			<View style={{ flex: 1 }}>
				<Text style={{color: Colors.Green}}>
					{nama}
				</Text>
				<Text>
					{id}
				</Text>
			</View>
		);
	}
}

export default Halaman;