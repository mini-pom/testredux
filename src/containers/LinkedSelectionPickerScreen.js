/**
 * Simple Linked Fetching
 */

import React, { Component } from "react";
import { View, Text, TouchableOpacity, Picker } from "react-native";
import { Actions } from "react-native-router-flux";
import { CustomPicker } from 'react-native-custom-picker'

import GlobalStyle from "../GlobalStyle";

const APIRoot = 'https://jasabox.com/api/v3/'

const APIGetProvince = APIRoot + 'locations/provinces'

const APIGetRegency = APIRoot + 'locations/regencies'

const APIGetDistrict = APIRoot + 'locations/districts'

class LinkedSelectionPickerScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			provinceList: [],
			regencyList: [],
			districtList: [],
			province: '',
			provinceName: '',
			regency: '',
			regencyName: '',
			district: '',
			districtName: '',
		}
	}

	componentDidMount() {
		this.fetchData(APIGetProvince, '', res => {
			this.setState({
				provinceList: res.result
			})
		})
	}

	fetchData(API, params_data = '', update) {
		return fetch(`${API}${params_data ? `?${params_data}` : ``}`, {
			method: 'GET',
			headers: {
				'Accept': 'multipart/form-data',
				'Content-Type': 'multipart/form-data',
			}
		})
			.then((response) => {
				const statusCode = response.status;
				const res = response.status == 200 ? response.json() : []
				return Promise.all([statusCode, res])
			})
			.then(([statusCode, result]) => {
				update(
					{
						"status": statusCode,
						"result": result
					}
				)
			})
			.catch(err => { throw err })
	}

	handleProvinceChange = (val) => {
		if (val) {
			this.setState({
				province: val.province_id,
				provinceName: val.province_name,
				regency: '',
				regencyName: '',
				district: '',
				districtName: ''
			})

			const params = `province_id=${val.province_id}`
			this.fetchData(APIGetRegency, params, res => {
				this.setState({
					regencyList: res.result
				})
			})
		}
		else {
			if(this.regencyPicker){
				this.regencyPicker.clear()
			}
			if(this.districtPicker){
				this.districtPicker.clear()
			}
			this.setState({
				province: '',
				provinceName: '',
				regency: '',
				regencyName: '',
				district: '',
				districtName: '',
				regencyList: [],
				districtList: []
			})
		}
	}

	handleRegencyChange = (val) => {
		if (val) {
			this.setState({
				regency: val.regency_id,
				regencyName: val.regency_name,
				district: '',
				districtName: ''
			})

			const params = `regency_id=${val.regency_id}`
			this.fetchData(APIGetDistrict, params, res => {
				this.setState({
					districtList: res.result
				})
			})
		}
		else {
			if(this.districtPicker){
				this.districtPicker.clear()
			}
			this.setState({
				regency: '',
				regencyName: '',
				district: '',
				districtName: '',
				districtList: []
			})
		}
	}

	handleDistrictChange = (val) => {
		if (val) {
			this.setState({
				district: val.district_id,
				districtName: val.district_name
			})
		}
		else {
			this.setState({
				district: '',
				districtName: ''
			})
		}
	}

	renderOption(settings) {
		const { item, getLabel } = settings
		return (
			<View style={{ padding: 16, borderTopColor: '#efefef', borderTopWidth: 2 }}>
				<Text style={{ color: '#000' }}>{getLabel(item)}</Text>
			</View>
		)
	}

	render() {
		return (
			<View style={GlobalStyle.container}>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
					<Text style={GlobalStyle.customBtnText}>Back</Text>
				</TouchableOpacity>
				<CustomPicker
					getLabel={item => item.province_name}
					optionTemplate={this.renderOption}
					options={this.state.provinceList}
					onValueChange={this.handleProvinceChange}
				/>
				<CustomPicker
					ref={ref => { this.regencyPicker = ref; }}
					getLabel={item => item.regency_name}
					optionTemplate={this.renderOption}
					options={this.state.regencyList}
					onValueChange={this.handleRegencyChange}
				/>
				<CustomPicker
					ref={ref => { this.districtPicker = ref; }}
					getLabel={item => item.district_name}
					optionTemplate={this.renderOption}
					options={this.state.districtList}
					onValueChange={this.handleDistrictChange}
				/>
				<View style={{ height: 200, justifyContent: 'space-around' }}>
					<Text>Chosen Province : {this.state.provinceName}</Text>
					<Text>Chosen Regency : {this.state.regencyName}</Text>
					<Text>Chosen District : {this.state.districtName}</Text>
				</View>
			</View>
		);
	}
}

export default LinkedSelectionPickerScreen;