import React, { Component } from "react";
import { View, Text, TouchableOpacity, Platform, Clipboard } from "react-native";
import { Actions } from "react-native-router-flux";
import GlobalStyle from "../GlobalStyle";
import firebase from "react-native-firebase"

class PushNotificationScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firebase_token: ''
		}
	}

	componentDidMount() {
		firebase.notifications().getScheduledNotifications().then(res => console.log(res))
		firebase.messaging().hasPermission()
			.then(enabled => {
				console.log(enabled)
				firebase.messaging().requestPermission()
					.then(() => {
						// console.log(data)
						firebase.messaging().getToken().then(firebase_token => {
							console.log(firebase_token)
							this.setState({
								firebase_token: firebase_token
							})
						})
					})
					.catch(error => {
						console.warn("Error" + error)
					});
			});
	}

	buildNotification = () => {
		// Build your notification
		const notification = new firebase.notifications.Notification()
			.setTitle('Event Attendance Confirmation')
			.setBody('SAP Anniversary 2019')
			.setNotificationId('attendance-1')
			.setSound('kid_laugh')
			.setData({
				"id": "1",
				"type": "important",
				"name": "james"
			})
			.android.setChannelId('attendance-1')
			.android.setPriority(firebase.notifications.Android.Priority.Max);

		// Build an action
		const attendAction = new firebase.notifications.Android.Action('attend', 'ic_launcher', 'Attend');
		const remindLaterAction = new firebase.notifications.Android.Action('remind', 'ic_launcher', 'Remind me later');

		// Build a remote input
		// const remoteInput = new firebase.notifications.Android.RemoteInput('inputText')
		// 	.setLabel('Message');

		// Add the remote input to the action
		// remindLaterAction.addRemoteInput(remoteInput);

		// Add the action to the notification
		notification.android.addAction(attendAction);
		notification.android.addAction(remindLaterAction);

		// Display the notification
		// firebase.notifications().displayNotification(notification);

		// Schedule the notification for 1 minute in the future
		const date = new Date();
		date.setSeconds(date.getSeconds() + 5);
		console.log(date)

		firebase.notifications().scheduleNotification(notification, {
			exact: true,
			fireDate: date.getTime()
		})
	};

	copyToken = () => {
		Clipboard.getString(this.state.firebase_token)
	}

	testPushNotification = () => {
		this.buildNotification()
	}

	render() {
		return (
			<View style={GlobalStyle.container}>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
					<Text style={GlobalStyle.customBtnText}>Back</Text>
				</TouchableOpacity>
				<View style={{ width: '100%', padding: 20 }}>
					<View style={{ width: '100%', borderWidth: 2, padding: 10 }}>
						<Text style={{ fontWeight: 'bold', fontSize: 12, marginBottom: 20 }}>Firebase Token</Text>
						<Text selectable numberOfLines={2}>{this.state.firebase_token}</Text>
						<TouchableOpacity onPress={this.copyToken} style={{ marginTop: 20 }}>
							<Text style={{ fontSize: 16, color: 'blue' }}>Copy Text</Text>
						</TouchableOpacity>
					</View>
				</View>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.testPushNotification}>
					<Text style={GlobalStyle.customBtnText}>Test Push Notification</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

export default PushNotificationScreen;