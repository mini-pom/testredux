import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput, NativeModules } from "react-native";
import { Actions } from "react-native-router-flux";

import GlobalStyle from "../GlobalStyle";

const SharedStorage = NativeModules.SharedStorage;

class WidgetScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentText: '',
			currentImage: 'https://stickershop.line-scdn.net/stickershop/v1/product/7982958/LINEStorePC/main.png;compress=true',
		}
	}

	componentDidMount() {

	}

	changeText = () => {
		SharedStorage.set(
			JSON.stringify({
				text: this.state.currentText,
				image: this.state.currentImage
			})
		);
	}

	render() {
		return (
			<View style={GlobalStyle.container}>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
					<Text style={GlobalStyle.customBtnText}>Back</Text>
				</TouchableOpacity>
				<View style={{ height: 70, width: '100%', paddingHorizontal: 20 }}>
					<View style={{ marginBottom: 5 }}>
						<Text>Text</Text>
					</View>
					<TextInput
						value={this.state.currentText}
						onChangeText={currentText => this.setState({ currentText })}
						style={{ flex: 1, borderWidth: 1 }}
						placeholder='type text here'
					/>
				</View>
				<View style={{ height: 70, width: '100%', paddingHorizontal: 20 }}>
					<View style={{ marginBottom: 5 }}>
						<Text>Image Uri</Text>
					</View>
					<TextInput
						value={this.state.currentImage}
						onChangeText={currentImage => this.setState({ currentImage })}
						style={{ flex: 1, borderWidth: 1 }}
						placeholder='type image here'
					/>
				</View>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.changeText}>
					<Text style={GlobalStyle.customBtnText}>Update Widget</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

export default WidgetScreen;