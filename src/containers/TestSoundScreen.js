import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";
import Sound from 'react-native-sound'
import GlobalStyle from "../GlobalStyle";

class TestSoundScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			soundArray: ['horn', 'evil_laugh', 'kid_laugh', 'tornado_siren'],
			activeSound: '',
			isActiveSound: false,
			currentDuration: '',
			maxDuration: '',
		}
	}

	soundListener = new Sound('')
	durationListener = null

	componentDidMount() {
		this.setState({
			activeSound: this.state.soundArray.find(e => e)
		}, () => {
			this.soundListener = new Sound(`${this.state.activeSound}.mp3`, null, (error) => {
				if (error) {
					// do something
				}
				// play when loaded
				this.setState({
					currentDuration: 0,
					maxDuration: Math.round(this.soundListener.getDuration()),
				})
			})
		})
	}

	changeSound = () => {
		var temp = ''
		this.soundListener.stop()
		do {
			temp = this.state.soundArray[Math.round(Math.random() * 3)]
			this.soundListener = new Sound(`${temp}.mp3`, null, (error) => {
				if (error) {
					// do something
				}
				this.setState({
					isActiveSound: false,
					activeSound: temp,
					currentDuration: 0,
					maxDuration: Math.round(this.soundListener.getDuration()),
				})
			})
		} while (temp == this.state.activeSound)
	}

	testSound = () => {
		if (this.soundListener.isPlaying()) {
			this.soundListener.stop()
			this.setState({
				isActiveSound: false,
				currentDuration: 0
			})
			clearInterval(this.durationListener)
		}
		else {
			this.setState({
				isActiveSound: true
			})
			this.durationListener = setInterval(() => {
				this.soundListener.getCurrentTime((sec, isPlaying) => {
					this.setState({
						currentDuration: Math.round(sec),
						maxDuration: Math.round(this.soundListener.getDuration()),
					})
				})
			}, 1000)

			// play when loaded
			this.soundListener.play();
		}
	}

	render() {
		return (
			<View style={GlobalStyle.container}>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
					<Text style={GlobalStyle.customBtnText}>Back</Text>
				</TouchableOpacity>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.changeSound}>
					<Text style={GlobalStyle.customBtnText}>Change Sound</Text>
				</TouchableOpacity>
				<Text>{this.state.currentDuration} : {this.state.maxDuration}</Text>
				<Text>Active Sound : {this.state.activeSound}</Text>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.testSound}>
					<Text style={GlobalStyle.customBtnText}>{this.state.isActiveSound ? 'Stop Sound' : 'Test Sound'}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

export default TestSoundScreen;