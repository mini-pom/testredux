import React, { Component, PureComponent } from "react";
import { View, Text, AsyncStorage } from 'react-native'

export default class CustomRightIndicator extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            signal: '0'
        }
    }

    componentDidMount() {
        this.updateStatus()
        this.statusInterval = setInterval(() => {
            this.updateStatus()
        }, 500);
    }

    componentWillUnmount() {
        clearInterval(this.statusInterval)
    }

    updateStatus = () => {
        AsyncStorage.getItem('interval_data', (err, res) => {
            this.setState({ signal: res })
        })
    }
    render() {
        const { signal } = this.state
        return (
            <View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 20 }}>{signal}</Text>
            </View>
        )
    }
}