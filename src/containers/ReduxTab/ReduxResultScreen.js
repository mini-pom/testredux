import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from 'react-redux';
import GlobalStyle from "../../GlobalStyle";

class ReduxResultScreen extends Component {
	_renderBtn = (title, operation) => {
		return (
			<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={operation}>
				<Text style={GlobalStyle.customBtnText}>{title}</Text>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={GlobalStyle.container}>
				<Text style={GlobalStyle.counter}>{this.props.counter}</Text>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		counter: state.counterOperation.counter
	};
};

const mapDispatchToProps = dispatch => {
	return {}
	// return {
	// 	add: by => {
	// 		dispatch(incrementBy(by));
	// 	},
	// 	remove: by => {
	// 		dispatch(decrementBy(by));
	// 	},
	// 	clear: () => dispatch(clear),
	// };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReduxResultScreen);