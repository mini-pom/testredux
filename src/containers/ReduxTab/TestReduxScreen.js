import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { incrementBy, decrementBy, clear } from "../../redux/actions/actionTypes";
import { connect } from 'react-redux';
import { Actions } from "react-native-router-flux";
import GlobalStyle from "../../GlobalStyle";

const factor = 1;

class TestReduxScreen extends Component {
	_renderBtn = (title, operation) => {
		return (
			<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={operation}>
				<Text style={GlobalStyle.customBtnText}>{title}</Text>
			</TouchableOpacity>
		);
	};

	render() {
		return (
			<View style={GlobalStyle.container}>
				{this._renderBtn("BACK", () => Actions.pop())}
				<View style={{ width: 200, flexDirection: 'row', justifyContent: 'space-around' }}>
					{this._renderBtn("-", this._decrement)}
					{this._renderBtn("+", this._increment)}
				</View>
				<Text style={{ fontSize: 32 }}>{this.props.counter}</Text>
				{this._renderBtn("clear", this._clear)}
			</View>
		);
	}

	_increment = () => {
		this.props.add(factor);
	};

	_decrement = () => {
		this.props.remove(factor);
	};

	_clear = () => {
		this.props.clear();
	};
}

const mapStateToProps = state => {
	return {
		counter: state.counterOperation.counter
	};
};

const mapDispatchToProps = dispatch => {
	return {
		add: by => {
			dispatch(incrementBy(by));
		},
		remove: by => {
			dispatch(decrementBy(by));
		},
		clear: () => dispatch(clear),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(TestReduxScreen);