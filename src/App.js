import React, { Component } from "react";
import { Alert, Text, View } from "react-native";
import { connect, Provider } from "react-redux";
import configureStore from "./redux/stores/store";
import NavigationRouter from "./navigations/NavigationRouter";
import firebase from "react-native-firebase"

const ConnectedRouter = connect()(NavigationRouter);
const store = configureStore();

class App extends Component {
	componentDidMount() {
		this.createNotificationChannel()
		this.createNotificationListeners();
	}

	createNotificationChannel = () => {
		// Build a android notification channel
		const channel = new firebase.notifications.Android.Channel(
			"attendance-1", // channelId
			"Test Channel", // channel name
			firebase.notifications.Android.Importance.High // channel importance
		).setDescription("Used for getting reminder notification"); // channel description
		// Create the android notification channel
		firebase.notifications().android.createChannel(channel);
	};

	async createNotificationListeners() {
		/*
		* Triggered when a particular notification has been received in foreground
		* */
		this.notificationListener = firebase.notifications().onNotification((notification) => {
			console.log(notification)
			let {
				body, // Notification Text
				data,  // Custom Data
				notificationId,
				sound,
				subtitle,
				title  //Notification Title
			} = notification;
			console.log(data)
			this.showAlert('App in Foreground', body);
		});

		/*
		* If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
		* */
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
			console.log(notificationOpen)
			let {
				body, // Notification Text
				data,  // Custom Data
				notificationId,
				sound,
				subtitle,
				title  //Notification Title
			} = notificationOpen.notification;
			console.log(data)
			this.showAlert('App from background', body);
		});

		/*
		* If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
		* */
		const notificationOpen = await firebase.notifications().getInitialNotification();
		if (notificationOpen) {
			console.log(notificationOpen)
			let {
				body, // Notification Text
				data,  // Custom Data
				notificationId,
				sound,
				subtitle,
				title  //Notification Title
			} = notificationOpen.notification;
			this.showAlert('App is Closed', body);
		}
		/*
		* Triggered for data only payload in foreground
		* */
		this.messageListener = firebase.messaging().onMessage((message) => {
			//process data message
			console.log(message);
		});

		// Set up your listener
		firebase.notifications().onNotificationOpened((notificationOpen) => {
			// notificationOpen.action will equal 'test_action'
			alert(notificationOpen.action)
			firebase.notifications().removeDeliveredNotification('event-reminder')
		});
	}

	buildNotification = () => {
		// Build your notification
		const notification = new firebase.notifications.Notification()
			.setTitle('Event Attendance Confirmation')
			.setBody('SAP Anniversary 2019')
			.setNotificationId('event-reminder')
			.setSound('kid_laugh')
			.setData({
				"id": "1",
				"type": "important",
				"name": "james"
			})
			.android.setChannelId('event-reminder')
			.android.setPriority(firebase.notifications.Android.Priority.Max);

		// Build an action
		const attendAction = new firebase.notifications.Android.Action('attend', 'ic_launcher', 'Attend');
		const remindLaterAction = new firebase.notifications.Android.Action('remind', 'ic_launcher', 'Remind me later');

		// Build a remote input
		const remoteInput = new firebase.notifications.Android.RemoteInput('inputText')
			.setLabel('Message');

		// Add the remote input to the action
		remindLaterAction.addRemoteInput(remoteInput);

		// Add the action to the notification
		notification.android.addAction(attendAction);
		notification.android.addAction(remindLaterAction);

		// Display the notification
		firebase.notifications().displayNotification(notification);
	};

	showAlert(title, body) {
		Alert.alert(
			title, body,
			[
				{ text: 'OK', onPress: () => console.log('OK Pressed') },
			],
			{ cancelable: false },
		);
	}

	render() {
		return (
			<Provider store={store}>
				<NavigationRouter />
			</Provider>
		);
	}
}

export default App;