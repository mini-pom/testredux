import React, { Component } from 'react';
import { Text, Animated, View, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { wait } from '../GlobalFunction';
import { Colors } from '../GlobalConfig';

const { height, width } = Dimensions.get('window')

export default class CustomLoading extends Component {
    static propTypes = {
        numberOfDots: PropTypes.number,
        animationDelay: PropTypes.number,
        minOpacity: PropTypes.number,
        style: Text.propTypes.style,
    };

    static defaultProps = {
        numberOfDots: parseInt(width / 20),
        animationDelay: 100,
        minOpacity: 0,
        style: {
            backgroundColor: Colors.PrimaryColor,
            height: 2,
            width: 8
        }
    };

    constructor(props) {
        super(props);

        this._animation_state = {
            dot_opacities: this.initializeDots(),
            target_opacity: 1,
            should_animate: true,
        };
    }

    initializeDots() {
        let opacities = [];

        for (let i = 0; i < this.props.numberOfDots; i++) {
            let dot = new Animated.Value(this.props.minOpacity);
            opacities = [...opacities, dot]
        }

        return opacities;
    }

    componentDidMount() {
        this.animate_dots.bind(this)(0);
    }

    componentWillUnmount() {
        this._animation_state.should_animate = false;
    }

    animate_dots(which_dot) {
        if (!this._animation_state.should_animate) return;

        if (which_dot >= this._animation_state.dot_opacities.length) {
            which_dot = 0;
        }

        let next_dot = which_dot + 1;

        Animated.timing(this._animation_state.dot_opacities[which_dot], {
            toValue: this._animation_state.target_opacity,
            duration: this.props.animationDelay,
        }).start(this.animate_dots.bind(this, next_dot));
        wait(1000).then(() => {
            Animated.timing(this._animation_state.dot_opacities[which_dot], {
                toValue: this.props.minOpacity + (next_dot * 10 / 500),
                duration: this.props.animationDelay + (next_dot * 50),
            }).start();
        })
    }

    render() {
        let dots = this._animation_state.dot_opacities.map((o, i) => (
            <Animated.View key={i} style={[this.props.style, { opacity: o }]} />
        ));

        return <View style={styles.container}>{dots}</View>
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});