var TENSION = 800;
var FRICTION = 90;

import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    View,
    Animated,
    PanResponder,
    Dimensions,
    StatusBar
} from 'react-native';
import { wait } from '../GlobalFunction';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const LOADING_DRAWER_HEIGHT = 160
/**
 * | refFunc | undefined | `function` | Can get the instance of the Drawer and use it's internal functions  |
 * | onDragDown | undefined | `function` | If you pass a function as parameter it will be notified when the user drag down the drawer  |
 * | onRelease | undefined | `function` | If you pass a function as parameter it will be notified when the user release the drawer after drag it |
 * | initialDrawerSize | 0.0 | `number` | It's the initial position or size for Drawer component. If  drawer size is 1 that means its using 100% of avalaible space on the screen |
 * | renderContainerView | undefined | `View` | Pass as parameter a renderable react component to show as container. |
 * | renderDrawerView | undefined | `View` | Pass as parameter a renderable react component to show as drawer. |
 * | renderInitDrawerView| undefined | `View` | Pass as parameter a renderable react component to show as view can draggable |
 */
export default class DraggableView extends Component {
    constructor(props) {
        super(props)
        // naming it initialX clearly indicates that the only purpose
        // of the passed down prop is to initialize something internally
        const drawerWithStatusBar = this.props.initialDrawerHeight + parseInt(StatusBar.currentHeight ? StatusBar.currentHeight : 0)
        const initialDrawerSize = SCREEN_HEIGHT - drawerWithStatusBar;

        const finalDrawerSize = this.props.finalDrawerHeight ? this.props.finalDrawerHeight : 0;

        this.props.refFunc(this);
        // console.log(initialDrawerSize, 'Initial size');
        this.state = {
            isDrawerSwiped: false,
            touched: 'FALSE',
            initialPositon: initialDrawerSize,
            finalPosition: finalDrawerSize,
            drawerWithStatusBar: drawerWithStatusBar,
            initialDrawerSize: initialDrawerSize,
            initialLoad: true
        }
        this.position = new Animated.Value(initialDrawerSize)
        this.animatedDrawer = new Animated.Value(0)
        this.panGesture = PanResponder.create({
            onMoveShouldSetPanResponder: (evt, gestureState) => {
                console.log(gestureState)
                // console.warn('in onMoveShouldSetPanResponder')
                return this.isAValidMovement(gestureState) && this.state.touched == 'TRUE'
            },
            onPanResponderMove: (evt, gestureState) => {
                // console.warn('in onPanResponderMove')
                this.setState({
                    isDrawerSwiped: true,
                })
                this.moveDrawerView(gestureState);
            },
            onPanResponderRelease: (evt, gestureState) => {
                // console.warn('in onPanResponderRelease')
                this.moveFinished(gestureState);
            },
        })
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.initialDrawerHeight != this.props.initialDrawerHeight) {
            const drawerWithStatusBar = this.props.initialDrawerHeight + parseInt(StatusBar.currentHeight ? StatusBar.currentHeight : 0)
            const initialDrawerSize = SCREEN_HEIGHT - drawerWithStatusBar
            const finalDrawerSize = this.props.finalDrawerHeight ? this.props.finalDrawerHeight : 0;

            if (this.props.initialDrawerHeight == LOADING_DRAWER_HEIGHT && this.state.initialLoad) {
                wait(1000).then(() => {
                    this.setState({
                        initialPositon: initialDrawerSize,
                        drawerWithStatusBar: drawerWithStatusBar,
                        finalPosition: finalDrawerSize,
                        initialDrawerSize: initialDrawerSize,
                        initialLoad: false
                    })
                    Animated.timing(this.position, {
                        toValue: initialDrawerSize,
                        duration: 500
                    }).start();
                })
            }
            else {
                this.setState({
                    initialPositon: initialDrawerSize,
                    drawerWithStatusBar: drawerWithStatusBar,
                    finalPosition: finalDrawerSize,
                    initialDrawerSize: initialDrawerSize,
                    initialLoad: true
                })
                Animated.timing(this.position, {
                    toValue: initialDrawerSize,
                    duration: 500
                }).start();
            }
        }
    }


    enterLoadingState() {
        this.startAnimation(-100, 0, this.state.finalPosition, null, this.state.initialPositon);
        this.props.onRelease && this.props.onRelease(false);
    }

    closeDrawer = () => {
        this.startAnimation(-100, 0, this.state.finalPosition, null, this.state.initialPositon);
        this.props.onRelease && this.props.onRelease(false); // only add this line if you need to detect if the drawer is up or not
    }

    openDrawer = async () => {
        this.startAnimation(-100, SCREEN_HEIGHT, this.state.initialPositon, null, this.state.finalPosition);
        this.props.onRelease && this.props.onRelease(true); // only add this line if you need to detect if the drawer is up or not
    }

    /**
     * Handle the Overscroll
     */
    isAValidMovement = (gestureState) => {
        const { moveX, moveY, vx, vy, dx, dy } = gestureState
        // moveX Touched X Coordinates
        // moveY Touched Y Coordinates
        // dy < 0 = prevent scroll top
        // dy > 0 = prevent scroll down

        // console.warn(this.state.initialDrawerSize)
        if (this.props.disableSwipe) return;

        // if (moveY < this.state.initialDrawerSize && (vy > 0 || dy < 0)) return;

        if (this.state.finalPosition > 100) {
            if (moveY < (this.state.initialDrawerSize + 100) && (dy < 0)) {
                console.log('stopped top')
                return;
            }
            if (moveY > (this.state.drawerWithStatusBar + 150) && (dy > 0)) {
                console.log('stopped bottom')
                return;
            }
        }
        else {
            if (moveY < this.state.initialDrawerSize && (dy < 0)) {
                console.log('stopped top')
                return;
            }
            if (moveY > this.state.drawerWithStatusBar && (dy > 0)) {
                console.log('stopped bottom')
                return;
            }
        }

        const moveTravelledFarEnough = Math.abs(dy) > Math.abs(dx) && Math.abs(dy) > 2;
        return moveTravelledFarEnough;
    }

    makePanResponderMoveOnTouch = () => {
        const {
            isDrawerSwiped,
        } = this.state
        const {
            onTouchDrawer
        } = this.props
        if (!isDrawerSwiped && onTouchDrawer) {
            onTouchDrawer()
        }
    }

    startAnimation = (velocityY, positionY, initialPositon, id, finalPosition) => {
        console.log('creating animation ');
        var isGoingToUp = (velocityY < 0) ? true : false;
        var speed = Math.abs(velocityY);
        var currentPosition = Math.abs(positionY / SCREEN_HEIGHT);
        var endPosition = isGoingToUp ? finalPosition : initialPositon;

        // position minus to fix stutter
        var position = new Animated.Value(isGoingToUp ? positionY - 100 : positionY - 50);
        position.removeAllListeners();

        // console.log('configuration : '+endPosition)
        Animated.timing(position, {
            toValue: endPosition,
            // tension: TENSION,
            // friction: FRICTION,
            // tension: 30,
            // friction: 0,
            duration: 500,
            // easing:Easing.elastic,
            velocity: velocityY,
            useNativeDriver: true
        }).start();

        Animated.timing(this.animatedDrawer, {
            toValue: isGoingToUp ? 1 : 0,
            duration: 500,
            useNativeDriver: true
        }).start();

        // position.addListener((position) => { console.log('position by', position, endPosition); });
        position.addListener((position) => {
            if (!this.center) return;
            this.onUpdatePosition(position.value);
        });
    }

    onUpdatePosition(position) {
        // console.log('UPDATE_POSITION', position);
        position = position;
        if (position < this.props.finalDrawerHeight) return;
        // 440 as on the desired drawer height + Google Logo
        if (position > 440) return;
        // position = position - 50;
        this.position.setValue(position);
        this._previousTop = position;
        // console.log('Position ', position);
        const { initialPosition } = this.state

        if (initialPosition === position) {
            this.props.onInitialPositionReached && this.props.onInitialPositionReached();
        }
    }

    moveDrawerView(gestureState) {
        console.log('GESTURE', gestureState);
        // console.log(gestureState.vy);
        if (!this.center) return;
        // var currentValue = Math.abs(gestureState.moveY / SCREEN_HEIGHT);
        // var isGoingToUp = (gestureState.vy < 0);
        //Here, I'm subtracting %5 of screen size from edge drawer position to be closer as possible to finger location when dragging the drawer view
        var position = gestureState.moveY - SCREEN_HEIGHT * 0.1;
        // var position = gestureState.moveY - SCREEN_HEIGHT * 0.05;
        //Send to callback function the current drawer position when drag down the drawer view component
        // if (!isGoingToUp) this.props.onDragDown(1 - currentValue);
        this.onUpdatePosition(position);
    }

    moveFinished(gestureState) {
        var isGoingToUp = (gestureState.vy < 0);
        if (!this.center) return;
        this.startAnimation(gestureState.vy, gestureState.moveY, this.state.initialPositon, gestureState.stateId, this.state.finalPosition);
        this.props.onRelease && this.props.onRelease(isGoingToUp);
    }

    render() {
        var containerView = this.props.renderContainerView ? this.props.renderContainerView() : null;
        var drawerView = this.props.renderDrawerView ? this.props.renderDrawerView() : null;
        var initDrawerView = this.props.renderInitDrawerView ? this.props.renderInitDrawerView() : null;
        var drawerPosition = {
            top: this.position
        };
        const mapOverlayOpacity = this.animatedDrawer.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0.6]
        })

        return (
            <View style={styles.viewport}>
                <View style={styles.container}>
                    {this.state.finalPosition < 100 &&
                        <Animated.View style={{ position: 'absolute', height: '100%', width: '100%', zIndex: 1, backgroundColor: '#000', opacity: mapOverlayOpacity }} />
                    }
                    {containerView}
                </View>
                <Animated.View
                    style={[drawerPosition, styles.drawer]}
                    ref={(center) => this.center = center}
                    {...this.panGesture.panHandlers}
                >
                    <TouchableWithoutFeedback
                        onPressIn={() => {
                            // console.warn('touch in');
                            this.setState({
                                touched: 'TRUE',
                            });
                        }}
                        onPressOut={() => {
                            this.setState({
                                touched: 'FALSE',
                                isDrawerSwiped: false,
                            });
                            this.makePanResponderMoveOnTouch()
                            // console.warn('touch out');
                        }}>
                        {initDrawerView}
                    </TouchableWithoutFeedback>
                    {drawerView}
                </Animated.View>
            </View >
        );
    }
};


var styles = StyleSheet.create({
    viewport: {
        flex: 1,
    },
    drawer: {
        flex: 1,
        zIndex: 2
    },
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});
