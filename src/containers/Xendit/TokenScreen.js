import React, { Component } from 'react';
import { CheckBox, Text, View, TextInput, TouchableHighlight, Clipboard, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';
import Xendit from 'xendit-js-node';

import styles from './styles';
import { XenditDevelopmentKey, convertPostMessageScript } from '../../GlobalConfig';

export default class TokenScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			amount: '70000',
			cardNumber: '4000000000000002',
			cardExpMonth: '12',
			cardExpYear: String(new Date().getFullYear() + 1),
			cardCvn: '123',
			isMultipleUse: false,
			isSkip3DS: false,
			isTokenizing: false,
			isRenderWebview: false,
			token: '',
			webviewUrl: ''
		}
	}

	onTextChange = (field) => value => this.setState({ [field]: value })

	setIsTokenizing() {
		this.setState({
			isTokenizing: !this.state.isTokenizing
		});
	}

	tokenize = () => {
		this.setIsTokenizing();
		Xendit.setPublishableKey(XenditDevelopmentKey);

		const tokenData = this.getTokenData();

		Xendit.card.createToken(tokenData, this.tokenResponseHandler);
	}

	getTokenData() {
		const {
			amount,
			cardNumber,
			cardExpMonth,
			cardExpYear,
			cardCvn,
			isMultipleUse,
			isSkip3DS
		} = this.state;

		return {
			amount,
			card_number: cardNumber,
			card_exp_month: cardExpMonth,
			card_exp_year: cardExpYear,
			card_cvn: cardCvn,
			is_multiple_use: isMultipleUse,
			should_authenticate: !isSkip3DS
		};
	}

	tokenResponseHandler = (err, token) => {
		if (err) {
			alert(JSON.stringify(err));
			this.setIsTokenizing();

			return;
		}
		console.log(token)

		switch (token.status) {
			case 'APPROVED':
			case 'FAILED':
				alert(JSON.stringify(token));
				break;
			case 'VERIFIED':
				this.setState({
					token: token.id
				}, () => {
					alert('Need Authentication for Multi use');
				});
				break;
			case 'IN_REVIEW':
				this.setState({
					webviewUrl: token.payer_authentication_url,
					isRenderWebview: true
				});
				break;
			default:
				alert('Unknown token status');
				break;
		}

		this.setIsTokenizing();
	}

	onMessage = (event) => {
		var parsedData = JSON.parse(event.nativeEvent.data)
		console.log(parsedData)

		this.setState({
			isRenderWebview: false,
			token: parsedData.id
		}, () => {
			alert(JSON.stringify(parsedData));
		});
	}

	render() {
		const {
			amount,
			cardNumber,
			cardExpMonth,
			cardExpYear,
			cardCvn,
			isMultipleUse,
			isSkip3DS,
			webviewUrl,
			isRenderWebview
		} = this.state;

		if (isRenderWebview) {
			return (
				<WebView
					injectedJavaScript={convertPostMessageScript}
					startInLoadingState
					scalesPageToFit={false}
					source={{ uri: webviewUrl }}
					onMessage={this.onMessage}
				/>
			)
		}

		return (
			<View style={styles.mainContainer}>
				<TextInput
					style={styles.textInput}
					placeholder="Amount"
					defaultValue={amount}
					onChangeText={this.onTextChange('amount')}
					keyboardType={'numeric'}
				/>
				<TextInput
					style={styles.textInput}
					placeholder="Card Number"
					maxLength={16}
					defaultValue={cardNumber}
					onChangeText={this.onTextChange('cardNumber')}
					keyboardType={'numeric'}
				/>
				<View style={styles.secondaryTextContainer}>
					<TextInput
						placeholder="Exp Month"
						maxLength={2}
						style={styles.secondaryTextInput}
						defaultValue={cardExpMonth}
						onChangeText={this.onTextChange('cardExpMonth')}
						keyboardType={'numeric'}
					/>
					<TextInput
						placeholder="Exp Year"
						maxLength={4}
						style={styles.secondaryTextInput}
						defaultValue={cardExpYear}
						onChangeText={this.onTextChange('cardExpYear')}
						keyboardType={'numeric'}
					/>
					<TextInput
						placeholder="CVN"
						maxLength={3}
						style={styles.secondaryTextInput}
						defaultValue={cardCvn}
						onChangeText={this.onTextChange('cardCvn')}
						keyboardType={'numeric'}
					/>
				</View>
				<View style={styles.checkBoxContainer}>
					<CheckBox
						value={isMultipleUse}
						onValueChange={this.onTextChange('isMultipleUse')}
					/>
					<Text style={styles.defaultContent}> Multiple use token? </Text>
				</View>
				<View style={styles.checkBoxContainer}>
					<CheckBox
						value={isSkip3DS}
						onValueChange={this.onTextChange('isSkip3DS')}
					/>
					<Text style={styles.defaultContent}> Skip authentication? </Text>
				</View>
				<TouchableHighlight
					style={styles.button}
					onPress={this.tokenize}
					disabled={this.state.isTokenizing}
				>
					<Text style={{ color: '#fff' }}>Tokenize</Text>
				</TouchableHighlight>

				{this.state.token != '' && (
					<TouchableOpacity onPress={() => Clipboard.setString(this.state.token)}>
						<View style={{ width: '100%', paddingVertical: 20 }}>
							<Text>{this.state.token}</Text>
						</View>
					</TouchableOpacity>
				)}
			</View>
		);
	}
}