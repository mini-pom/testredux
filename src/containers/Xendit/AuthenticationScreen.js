import React, { Component } from 'react';
import { Text, View, TextInput, TouchableHighlight } from 'react-native';
import { WebView } from 'react-native-webview';
import Xendit from 'xendit-js-node';

import styles from './styles';
import { convertPostMessageScript, XenditDevelopmentKey } from '../../GlobalConfig';

export default class AuthenticationScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			amount: '70000',
			tokenId: "5df8b38115e99e428efa04bb",
			isAuthenticating: false,
			isRenderWebview: false,
			webviewUrl: ''
		}
	}

	setIsAuthenticating() {
		this.setState({
			isAuthenticating: !this.state.isAuthenticating
		});
	}

	authenticate = () => {
		const {
			tokenId,
			amount
		} = this.state;

		this.setIsAuthenticating();

		Xendit.setPublishableKey(XenditDevelopmentKey);

		const threeDSData = {
			amount,
			token_id: tokenId
		};

		Xendit.card.createAuthentication(threeDSData, this.tokenResponseHandler);
	}

	tokenResponseHandler = (err, token) => {
		if (err) {
			alert(JSON.stringify(err));
			return;
		}
		console.log(token)

		switch (token.status) {
			case 'APPROVED':
			case 'VERIFIED':
			case 'FAILED':
				alert(JSON.stringify(token));
				break;
			case 'IN_REVIEW':
				this.setState({
					webviewUrl: token.payer_authentication_url,
					isRenderWebview: true
				});

				break;
			default:
				alert('Unknown token status');
				break;
		}

		this.setIsAuthenticating();
	}

	onMessage = (event) => {
		var parsedData = JSON.parse(event.nativeEvent.data)
		console.log(parsedData)

		this.setState({
			isRenderWebview: false
		}, () => {
			alert(JSON.stringify(parsedData));
		});
	}

	render() {
		const {
			amount,
			tokenId,
			webviewUrl,
			isRenderWebview,
			isAuthenticating
		} = this.state;

		if (isRenderWebview) {
			return (
				<WebView
					injectedJavaScript={convertPostMessageScript}
					startInLoadingState
					scalesPageToFit={false}
					source={{ uri: webviewUrl }}
					onMessage={this.onMessage}
				/>
			)
		}

		return (
			<View style={styles.mainContainer}>
				<TextInput
					style={styles.textInput}
					placeholder="Amount"
					defaultValue={amount}
					onChangeText={amount => this.setState({ amount: amount })}
					keyboardType={'numeric'}
				/>
				<TextInput
					style={styles.textInput}
					placeholder="Token ID"
					maxLength={24}
					defaultValue={tokenId}
					onChangeText={tokenId => this.setState({ tokenId: tokenId })}
					keyboardType={'numeric'}
				/>
				<TouchableHighlight
					style={styles.button}
					onPress={this.authenticate}
					disabled={isAuthenticating}
				>
					<Text style={{ color: '#fff' }}>Authenticate</Text>
				</TouchableHighlight>
			</View>
		);
	}
}