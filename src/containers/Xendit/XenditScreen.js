import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, NativeModules } from 'react-native';
import { Actions } from "react-native-router-flux";

const PUBLIC_DEVELOPMENT_KEY = 'xnd_development_JcAdnj9o8d3twsPlHvCOwSdyz6cKWmKg5W4SCP07xzoJ25QOGbVZ1ZKiACJT'

const PUBLISHABLE_KEY = 'xnd_public_development_SfheJrnzkcRcxgfwPoP7U0LabDmu2ZDgJntRkm33xRSYEIAkeLUHbbqjICDYVE6'

export default class XenditScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			API: '70000',
		}
	}
	componentDidMount() {

	}

	render() {
		return (
			<View style={styles.container}>
				<TouchableHighlight
					style={styles.option}
					onPress={() => Actions.xenditToken()}
				>
					<Text> Create Token </Text>
				</TouchableHighlight>
				<TouchableHighlight
					style={styles.option}
					onPress={() => Actions.xenditAuth()}
				>
					<Text> Create Authentication </Text>
				</TouchableHighlight>
				<TouchableHighlight
					style={styles.option}
					onPress={this.testAPI}
				>
					<Text> Test API </Text>
				</TouchableHighlight>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
	option: {
		padding: 20,
		backgroundColor: '#eef',
		marginTop: 20
	}
});