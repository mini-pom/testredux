import ACTION_TYPES from "../actions";
import { Actions } from "react-native-router-flux";

const initialState = {
  counter: 0
};

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.INCREMENT:
      Actions.refresh()
      return {
        ...state,
        counter: Number(state.counter) + Number(action.payload)
      };
    case ACTION_TYPES.DECREMENT:
      Actions.refresh()
      return {
        ...state,
        counter: Number(state.counter) - Number(action.payload)
      };
    case ACTION_TYPES.CLEAR:
      Actions.refresh()
      return {
        ...state,
        counter: Number(0)
      };
    default:
      return state;
  }
};

export default counterReducer;