import React, { Component } from "react";
import { View, Text, TouchableOpacity, ToastAndroid } from "react-native";
import { Actions } from "react-native-router-flux";
import GlobalStyle from "../GlobalStyle";
import Modal from "react-native-modal";
import { LinkedInClientID, LinkedInClientSecret, LinkedInRedirectURL, convertPostMessageScript } from "../GlobalConfig";
import WebView from "react-native-webview";

class LinkedInScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			random: '',
			isVisibleModal: false,
			linkedInLoginUrl: '',
			linkedInHTML: '',
			injectedJS: ''
		}
	}

	generateUniqueID() {
		const date = new Date
		const random = Math.random().toString(16).slice(-6);
		const mixedRandom = `${date.getTime()}x${random}`
		this.setState({
			random: mixedRandom
		})
		return mixedRandom
	}

	linkedInLogin = () => {
		const API = 'https://www.linkedin.com/oauth/v2/authorization'
		const responseType = 'response_type=code&'
		const clientId = 'client_id=81g2ijy5a8555j&'
		const redirectUri = 'redirect_uri=https://maelsov.id/wcs_website/auth/linkedin_callback&'
		const state = `state=${this.generateUniqueID()}&`
		const scope = 'scope=r_liteprofile%20r_emailaddress%20w_member_social'
		this.setState({
			linkedInLoginUrl: `${API}?${responseType}${clientId}${redirectUri}${state}${scope}`,
			isVisibleModal: true
		})
	}
	linkedInLogout = () => {
		this.linkedInRef.logoutAsync()
	}

	isJSON(str) {
		try {
			return (JSON.parse(str) && !!str);
		} catch (e) {
			return false;
		}
	}

	onMessage = (event) => {
		const data = event.nativeEvent.data

		if (this.isJSON(data)) {
			this.setState({
				linkedInLoginUrl: '',
				isVisibleModal: false
			})
			console.log(JSON.parse(data))
			console.log(data)
			alert(data)
		}
	}

	_onNavigationStateChange(webViewState) {
		console.log(webViewState)
		// this.setState({
		// 	linkedInLoginUrl: webViewState.url
		// })
	}
	updateInjectedJavaScript = () => {
		console.log('run')
		this.setState({
			injectedJS: 'window.ReactNativeWebView.postMessage(document.body.innerHTML)'
		})
	}
	// Cancel login
	// https://maelsov.id/wcs_website/auth/linkedin_callback?error=user_cancelled_login&error_description=The+user+cancelled+LinkedIn+login&state=1578342291341xf4afa7
	// Login Success 1st time
	// https://www.linkedin.com/oauth/v2/login-success?app_id=10299945&auth_type=AC&flow=%7B%22authorizationType%22%3A%22OAUTH2_AUTHORIZATION_CODE%22%2C%22redirectUri%22%3A%22https%3A%2F%2Fmaelsov.id%2Fwcs_website%2Fauth%2Flinkedin_callback%22%2C%22externalBindingKey%22%3Anull%2C%22loginHint%22%3Anull%2C%22codeChallenge%22%3Anull%2C%22codeChallengeMethod%22%3Anull%2C%22currentStage%22%3A%22LOGIN_SUCCESS%22%2C%22currentSubStage%22%3A0%2C%22flowHint%22%3Anull%2C%22authFlowName%22%3A%22generic-permission-list%22%2C%22appId%22%3A10299945%2C%22creationTime%22%3A1578342333912%2C%22state%22%3A%221578342333780x875e76%22%2C%22scope%22%3A%22r_liteprofile+r_emailaddress+w_member_social%22%7D
	// Login Allow Authorize
	// https://maelsov.id/wcs_website/auth/linkedin_callback?code=AQQuQSgkrw8wXUBKwXT3zzsM3kZb1JLjlDW3AKkEN6Nz6rmNYFYPo1pvBTkbCmz08oe7y6i2QFZdmN3lTv17M4Rv1vEw-z9JZZiqXyzbIiW26gVDF3N1-vZEUX2ymamFt_96M10w8hpW4o064_mhBbDgbPYzldIjXT8ieIg6q-Y389TL8bTtfY9NNfOYWQ&state=1578342569017x7c65b6
	// Login Allow Authorize 2nd time
	// "https://maelsov.id/wcs_website/auth/linkedin_callback?code=AQR9-RmM9a7PyPpNoqfcEWnrMX0MxZevlqcnR2mTi5bDcQis1ibYDZhRglqFTiiLn4WAXaKNYA5Jsv9HvIwwGYqJKagZSePdF9nCAeI4PNQ-SUvSwE_3F1aQxYalm2EVPN3uUHlT9S7wMlA_jvE4n2wezz6e9VZzMU39bI3KaaO9MS4fVHKjlewTXJjm0A&state=1578343595376x36e3d4"
	// Login but User Cancel Authorize
	// https://maelsov.id/wcs_website/auth/linkedin_callback?error=user_cancelled_authorize&error_description=The+user+cancelled+the+authorization&state=1578342333780x875e76
	// User Login After Cancel Authorize
	// https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81g2ijy5a8555j&redirect_uri=https://maelsov.id/wcs_website/auth/linkedin_callback&state=1578342569017x7c65b6&scope=r_liteprofile%20r_emailaddress%20w_member_social


	render() {
		const modalContent = (
			<View style={{ flex: 1, padding: 20 }}>
				<View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20, padding: 3 }}>
					<View style={{ flex: 1, borderWidth: 3, borderColor: '#000', borderRadius: 20, overflow: 'hidden' }}>
						<WebView
							injectedJavaScript={this.state.injectedJS}
							onLoad={this.updateInjectedJavaScript}
							source={{
								uri: this.state.linkedInLoginUrl
							}}
							cacheEnabled={false}
							incognito
							onMessage={this.onMessage}
							onNavigationStateChange={this._onNavigationStateChange.bind(this)}
						/>
					</View>
				</View>
			</View>
		)

		return (
			<View style={GlobalStyle.container}>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
					<Text style={GlobalStyle.customBtnText}>Back</Text>
				</TouchableOpacity>
				<Text>{this.state.random}</Text>
				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.linkedInLogin}>
					<Text style={GlobalStyle.customBtnText}>LinkedIn Login</Text>
				</TouchableOpacity>
				<Modal
					onBackdropPress={() => this.setState({ isVisibleModal: false })}
					children={modalContent}
					isVisible={this.state.isVisibleModal} />
			</View>
		);
	}
}
// class LinkedInScreen extends Component {
// 	constructor(props) {
// 		super(props);
// 		this.state = {
// 		}
// 	}

// 	linkedInLogin = () => {
// 		this.linkedInRef.open()
// 	}
// 	linkedInLogout = () => {
// 		this.linkedInRef.logoutAsync()
// 	}

// 	render() {
// 		return (
// 			<View style={GlobalStyle.container}>
// 				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={() => Actions.pop()}>
// 					<Text style={GlobalStyle.customBtnText}>Back</Text>
// 				</TouchableOpacity>
// 				<Text>{this.state.loggedIn ? 'LinkedIn Integrated' : 'Not Integrated'}</Text>
// 				<LinkedInModal
// 					renderButton={() => (
// 						<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.linkedInLogin}>
// 							<Text style={GlobalStyle.customBtnText}>LinkedIn Login</Text>
// 						</TouchableOpacity>
// 					)}
// 					ref={ref => { this.linkedInRef = ref }}
// 					clientID={LinkedInClientID}
// 					clientSecret={LinkedInClientSecret}
// 					redirectUri={LinkedInRedirectURL}
// 					onSuccess={token => console.log(token)}
// 				/>
// 				<TouchableOpacity style={GlobalStyle.customBtnBG} onPress={this.linkedInLogout}>
// 					<Text style={GlobalStyle.customBtnText}>LinkedIn Logout</Text>
// 				</TouchableOpacity>
// 			</View>
// 		);
// 	}
// }

export default LinkedInScreen;