import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput, NativeModules } from "react-native";
import { Actions } from "react-native-router-flux";

import GlobalStyle from "../GlobalStyle";
import { watchPosition } from "react-native-geolocation-service";

const SharedStorage = NativeModules.SharedStorage;

class WidgetScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [
				'asd',
				'def',
				'ghi'
			]
		}
	}

	/** Di Halaman Confirm */
	confirmLoc=(locnow)=> {
		Actions.pop()
		setTimeout(()=>Actions.refresh
		({
			updatedLoc:locnow
		}), 0)
	}

	render() {
		const { data } = this.state
		return (
			<View style={GlobalStyle.container}>
				{data.map((item, index) => {
					return (
						<TouchableOpacity style={{ height: 40, width: 200, backgroundColor: 'cyan', justifyContent: 'center', alignItems: 'center', marginBottom: 20 }} onPress={() => this.confirmLoc(item)}>
							<Text>{item}</Text>
						</TouchableOpacity>
					)
				})}
			</View>
		);
	}
}

export default WidgetScreen;