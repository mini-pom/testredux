const APIROOT = 'http://dummy.restapiexample.com/api/v1/'

export const APIGetEmployee = `${APIROOT}employees`
export const APIGetEmployeeDetail = `${APIROOT}employee/`