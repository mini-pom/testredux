import React, { Component } from "react";
import { View, Text, TouchableOpacity, ScrollView, StyleSheet, AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import GlobalStyle from "../GlobalStyle";
import { connect } from 'react-redux';
import firebase from "react-native-firebase"

class MenuScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	componentDidMount() {
		firebase.messaging().hasPermission()
			.then(enabled => {
				console.log(enabled)
				firebase.messaging().requestPermission()
					.then(() => {
					})
			});

		this.statusInterval = setInterval(() => {
			this.changeStatus()
		}, 500);
	}

	componentWillUnmount() {
		clearInterval(this.statusInterval)
	}

	changeStatus = async () => {
		const status = await AsyncStorage.getItem('interval_data')
		if (status < 10) AsyncStorage.setItem('interval_data', String(parseInt(status) + 1))
		else AsyncStorage.setItem('interval_data', '0')
	}

	startLinkedIn = () => {
		Actions.linkedInTest()
	}

	onSuccessRegister = token => {
		console.log(token)
	}

	onFailedRegister = error => {
		console.log(error)
	}

	render() {
		return (
			<View style={styles.container}>
				<ScrollView contentContainerStyle={{ paddingVertical: 20 }}>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.testSound()}>
						<Text style={GlobalStyle.customBtnText}>Test Sound</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.widget()}>
						<Text style={GlobalStyle.customBtnText}>Update Widget</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.xendit()}>
						<Text style={GlobalStyle.customBtnText}>Xendit</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.pushNotification()}>
						<Text style={GlobalStyle.customBtnText}>Push Notification</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.linkedSelection()}>
						<Text style={GlobalStyle.customBtnText}>Linked Selection</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.faceRecognition()}>
						<Text style={GlobalStyle.customBtnText}>Face Recognition</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.demoMap()}>
						<Text style={GlobalStyle.customBtnText}>Map</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.drawerRoot()}>
						<Text style={GlobalStyle.customBtnText}>Drawer</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.menuButton} onPress={() => Actions.tab()}>
						<Text style={GlobalStyle.customBtnText}>Redux Tester{this.props.counter ? `(${this.props.counter})` : ''}</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	menuButton: {
		...GlobalStyle.customBtnBG,
		marginVertical: 20
	}
})

const mapStateToProps = state => {
	return {
		counter: state.counterOperation.counter
	};
};

const mapDispatchToProps = dispatch => {
	return {
		// add: by => {
		// 	dispatch(incrementBy(by));
		// },
		// remove: by => {
		// 	dispatch(decrementBy(by));
		// },
		// clear: () => dispatch(clear),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);