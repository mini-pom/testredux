import { createStore, combineReducers, applyMiddleware } from "redux";
import counterReducer from "../reducers/counterReducer";
import logger from 'redux-logger'

// Place to store the variable
const rootReducer = combineReducers({
  counterOperation: counterReducer
});

const configureStore = (() => {
  return createStore(
    rootReducer,
    applyMiddleware(logger)
  );
});

export default configureStore;