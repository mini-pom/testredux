import React, { Component, PureComponent } from "react";
import { View, Text, TouchableOpacity, TouchableNativeFeedback, StyleSheet, PermissionsAndroid, Image, Dimensions, TextInput, TouchableWithoutFeedback, Keyboard, Animated, FlatList, StatusBar, ActivityIndicator, Linking } from "react-native";
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import Geolocation from 'react-native-geolocation-service';
import { wait, call, sms } from "../GlobalFunction";
import DraggableView from "../components/DraggableView";
import CustomLoading from "../components/CustomLoading";
import CustomCancelModal from "../components/CustomCancelModal";
import CustomButton from "../components/CustomButton";
import Modal from 'react-native-modal';
import { Icons } from "../GlobalConfig";

const NAVBAR_HEIGHT = 56
const GOOGLE_LOGO_HEIGHT = 30

const LOADING_DRAWER_HEIGHT = 160
const CHOOSE_LOCATION_DRAWER_HEIGHT = 200
const PRE_ORDER_DRAWER_HEIGHT = 320
const MATCHMAKING_DRAWER_HEIGHT = 230
const ORDER_PROGRESS_DRAWER_HEIGHT = 260

const { height, width } = Dimensions.get('window')

const PAYMENT_TYPE = Object.freeze({
	ALPHA_WALLET: 'ALPHA_WALLET',
	CASH: 'CASH'
})

const SERVICE_TYPE = Object.freeze({
	RIDE_BIKE: 'RIDE_BIKE',
	RIDE_CAR: 'RIDE_CAR'
})

const LOCATION_TYPE = Object.freeze({
	PICK_UP: 'PICK_UP',
	DESTINATION: 'DESTINATION'
})

const STATE_LIST = {
	LOADING_STATE: 'LOADING_STATE',
	CHOOSE_LOCATION_STATE: 'CHOOSE_LOCATION_STATE',
	CHOOSE_FROM_MAP_STATE: 'CHOOSE_FROM_MAP_STATE',
	PRE_ORDER_STATE: 'PRE_ORDER_STATE',
	MATCHMAKING_STATE: 'MATCHMAKING_STATE',
	ORDER_PROGRESS_STATE: 'ORDER_PROGRESS_STATE',
}

const Colors = Object.freeze({
	PrimaryColor: '#5dbb2c',
	WhiteColor: '#fff',
	BlackColor: '#000',
	GreyColor: 'grey',
	SemiGreyColor: '#e2e2e2',
	LightGreyColor: '#fbfbfb',
	PurpleColor: '#eef',
	BlackFontColor: '#1c1c1c',
})

const Currencies = Object.freeze({
	Naira: '₦',
})

class PaymentMethodItem extends PureComponent {
	render() {
		const { item, index, changePaymentMethod } = this.props
		let activePaymentMethodName = ''
		let activePaymentMethodImage = ''
		switch (item) {
			case PAYMENT_TYPE.ALPHA_WALLET:
				activePaymentMethodName = 'Alpha Wallet'
				activePaymentMethodImage = Icons.iconWallet
				break;
			case PAYMENT_TYPE.CASH:
				activePaymentMethodName = 'Pay cash'
				activePaymentMethodImage = Icons.iconCash
				break;
			default:
				break;
		}
		return (
			<TouchableOpacity onPress={() => changePaymentMethod(item)} style={{ height: 50, width: '100%', flexDirection: 'row', alignItems: 'center', borderTopWidth: 1, borderTopColor: Colors.SemiGreyColor }}>
				<View style={{ height: 22, width: 22, marginRight: 12 }}>
					<Image style={styles.staticImageSize} source={activePaymentMethodImage} />
				</View>
				<Text style={{ fontWeight: 'bold', fontSize: 10, letterSpacing: 0.5 }}>{activePaymentMethodName}</Text>
			</TouchableOpacity>
		)
	}
}

class MarkerItem extends PureComponent {
	render() {
		const { imageSource, bigger } = this.props
		return (
			<View style={{ height: bigger ? 64 : 36, aspectRatio: 1 }}>
				<Image source={imageSource} resizeMode='contain' style={styles.staticImageSize} />
			</View>
		)
	}
}

class LocationItem extends PureComponent {
	render() {
		const { item, chooseDestinationLocation, index } = this.props
		return (
			<TouchableOpacity
				onPress={chooseDestinationLocation(item)}
				style={{ width: "100%", backgroundColor: Colors.GreyLightColor, flexDirection: 'row', paddingLeft: 22, paddingRight: 36, paddingBottom: 12 }}>
				<View style={{ height: 24, aspectRatio: 1, marginRight: 16, marginTop: 12 }}>
					<Image style={{ width: '100%', height: '100%' }} resizeMode='contain' source={Icons.iconLocation2} />
				</View>
				<View style={[{ flex: 1, paddingTop: 12, borderTopColor: Colors.SemiGreyColor }, index != 0 && { borderTopWidth: 1 }]}>
					<View style={{ width: '100%', justifyContent: 'center', marginBottom: 6 }}	>
						<Text style={{ fontSize: 16, fontWeight: 'bold' }}>{item.locationName}</Text>
					</View>
					<Text style={{ fontSize: 10, lineHeight: 16 }} numberOfLines={3}>{item.locationDesc}</Text>
				</View>
			</TouchableOpacity >
		)
	}
}

class DemoMapScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// Initial Map Zoom Lat long
			initialRegion: this.regionFrom(
				-6.189,
				106.896,
				100
			),

			// The recorded location when user drags the map
			region: null,
			showMarker: false,

			serviceType: SERVICE_TYPE.RIDE_BIKE,

			// Driver Vehicle Marker
			vehicleMarker: [
				{
					latlng: {
						latitude: -6.1895,
						longitude: 106.8965,
						latitudeDelta: 0.03196738289905454,
						longitudeDelta: 0.02748962491750717,
					}
				},
				{
					latlng: {
						latitude: -6.18953,
						longitude: 106.8967,
						latitudeDelta: 0.014861832052947221,
						longitudeDelta: 0.011971034109592438,
					}
				},
				{
					latlng: {
						latitude: -6.18923,
						longitude: 106.8955,
						latitudeDelta: 0.0017935139279599,
						longitudeDelta: 0.0014098361134529114,
					}
				}
			],

			// Location text input 
			locationType: LOCATION_TYPE.DESTINATION,
			locationInput: '',
			locationInputFocused: false,
			showLocationList: true,

			locationDescription: 'Jl Baru mulungan perkasa no 18, Blok A2-C2, Glingingan kec. Mlati, Sleman Daerah Istimewah Yogyakarta, Indonesia 33378',

			// Location Data after searching
			searchLocationList: [
				{
					locationName: "Kolam Renang Tirta Mas",
					locationDesc: "Jl. Kayu Putih Empat, RT.5/RW.6, Pulo Gadung, Kec. Pulo Gadung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13260",
					locationLatLng: {
						latitude: -6.185997612187556,
						longitude: 106.89204627647996,
						latitudeDelta: 0.03196738289905454,
						longitudeDelta: 0.02748962491750717,
					}
				},
				{
					locationName: "Rumah Sakit Columbia Asia Pulomas",
					locationDesc: "Jl Baru mulungan perkasa no 16, Blok A2-C2, Glingingan kec. Mlati, Sleman Daerah Istimewah Yogyakarta, Indonesia 33378",
					locationLatLng: {
						latitude: -6.18238736935,
						longitude: 106.8911162205,
						latitudeDelta: 0.03196738289905454,
						longitudeDelta: 0.02748962491750717,
					}
				},
				{
					locationName: "Jakarta Internasional Velodrome",
					locationDesc: "Jl Baru mulungan no 18, Glingingan kec. Mlati, sleman Daerah Istimewah Yogyakarta, Indonesia 33378",
					locationLatLng: {
						latitude: -6.19106877579,
						longitude: 106.8901445902,
						latitudeDelta: 0.03196738289905454,
						longitudeDelta: 0.02748962491750717,
					}
				}
			],


			// List of destination data 
			destinationName: 'Kolam Renang Tirta Mas',
			destinationLatLong: null,
			pickupName: 'Royal Catering',
			pickupLatLong: {
				latitude: -6.189,
				longitude: 106.896,
				latitudeDelta: 0.03196738289905454,
				longitudeDelta: 0.02748962491750717,
			},
			estimatePrice: 40,
			estimateDistance: 17.2,
			activePromo: '',

			// List of Payment Method State
			paymentMethodList: [
				PAYMENT_TYPE.ALPHA_WALLET,
				PAYMENT_TYPE.CASH
			],
			activePaymentMethod: PAYMENT_TYPE.ALPHA_WALLET,
			paymentMethodModalVisible: false,

			// List of Cancel Order State
			cancelOptionList: [
				'I have waited too long',
				'I have made another plan',
				'I want to change location',
				'Driver is unreachable',
				'Driver asked to cancel',
			],
			isLoadingCancel: false,
			cancelModalVisible: false,

			// Order Progress State
			currentOrderStatus: 'Driver is Heading to Customer for Pickup',
			driverPhoto: 'https://pbs.twimg.com/profile_images/930739502506823680/TSfzH-kY_400x400.jpg',
			driverName: 'Mario Newton',
			driverPlate: 'B 4637 HG',
			driverNumber: '085123123123',

			initialLoad: true,
			drawerOpen: false,
			isLoading: false,
			disableSwipe: false,

			// Bottom UI state is according to this
			activeState: STATE_LIST.CHOOSE_LOCATION_STATE
		}
		this.animatedDestinations = new Animated.Value(1)
		this.animatedMarker = new Animated.Value(1)
	}

	componentDidMount() {
		// this.requestLocationPermission()
		this.getCurrentPosition()
		wait(1000).then(() => {
			this.setState({
				initialLoad: false
			}, () => {
				wait(1000).then(() => this.animateMapTo(this.state.initialRegion))
			})
		})
	}

	onTextChange = (field) => (value) => this.setState({ [field]: value })

	/**
	 * Location Request Permission
	 */
	async requestLocationPermission() {
		const granted = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
			{
				title: 'Location Permission',
				message: 'Allow apps to get location permission'
			}
		)
		return granted === PermissionsAndroid.RESULTS.GRANTED
	}

	regionFrom(lat, lon, distance) {
		distance = distance / 2
		const circumference = 40075
		const oneDegreeOfLatitudeInMeters = 111.32 * 1000
		const angularDistance = distance / circumference

		const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
		const longitudeDelta = Math.abs(Math.atan2(
			Math.sin(angularDistance) * Math.cos(lat),
			Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)))

		return {
			latitude: lat,
			longitude: lon,
			latitudeDelta: 1,
			longitudeDelta: 1,
			// latitudeDelta: latitudeDelta,
			// longitudeDelta: longitudeDelta,
		}
	}

	getCurrentPosition = () => {
		if (PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)) {
			Geolocation.getCurrentPosition(
				(position) => {
					// this.setState({
					// 	initialRegion: this.regionFrom(
					// 		position.coords.latitude,
					// 		position.coords.longitude,
					// 		100
					// 	)
					// })
				},
				(error) => {
					// See error code charts below.
					console.log(error.code, error.message);
				},
				{ enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
			);
		}
		else {
			this.requestLocationPermission()
		}
	}

	/**
	 * Calculate the zoom level of the maps
	 * @param {Map Boundaries Props} bounds 
	 * @param {Height & Width} mapDim 
	 */
	getBoundsZoomLevel(bounds, mapDim) {
		var WORLD_DIM = { height: 256, width: 256 };
		var ZOOM_MAX = 21;

		function latRad(lat) {
			var sin = Math.sin(lat * Math.PI / 180);
			var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
			return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
		}

		function zoom(mapPx, worldPx, fraction) {
			return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
		}

		var ne = bounds.northEast;
		var sw = bounds.southWest;

		var latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

		var lngDiff = ne.longitude - sw.longitude;
		var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

		var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
		var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

		return Math.min(latZoom, lngZoom, ZOOM_MAX);
	}

	// getLatLonDiffInMeters(lat1, lon1, lat2, lon2) {
	// 	var R = 6371; // radius of the earth in km
	// 	var dLat = deg2rad(lat2 - lat1);  // deg2rad below
	// 	var dLon = deg2rad(lon2 - lon1);
	// 	var a =
	// 		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	// 		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
	// 		Math.sin(dLon / 2) * Math.sin(dLon / 2)
	// 		;
	// 	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	// 	var d = R * c; // distance in km
	// 	return d * 1000;
	// }

	animateMapTo = (latlongA, latlongB = null) => {
		let finalLat, finalLong
		if (latlongB) {
			finalLat = (latlongA.latitude + latlongB.latitude) / 2
			finalLong = (latlongA.longitude + latlongB.longitude) / 2
			this.mapView.getMapBoundaries().then(res => {
				const zoom = this.getBoundsZoomLevel(res, {
					height: height - 400,
					width: width - 100
				})
				this.mapView.animateCamera({
					center: {
						// latitude: parseInt(this.state.initialRegion.latitude),
						// longitude: parseInt(this.state.initialRegion.longitude),
						latitude: latlongB ? finalLat - 0.001 : latlongA.latitude,
						longitude: latlongB ? finalLong : latlongA.longitude,
					},
					// pitch: 2,
					// heading: 20,
					// altitude: 200,
					zoom: zoom
				}, { duration: 1000 })
			})
		}
		else {
			this.mapView.animateCamera({
				center: {
					// latitude: parseInt(this.state.initialRegion.latitude),
					// longitude: parseInt(this.state.initialRegion.longitude),
					latitude: latlongA.latitude,
					longitude: latlongA.longitude,
				},
				// pitch: 2,
				// heading: 20,
				// altitude: 200,
				zoom: 17
			}, { duration: 1000 })
		}
	}

	onRegionChange = region => {
		this.setState({
			region
		})
		this.mapView.getMapBoundaries().then(res => {
			const zoom = this.getBoundsZoomLevel(res, {
				height: height - 400,
				width: width - 100
			})
			// Smaller zoom is further
			wait(500).then(() => {
				this.setState({
					showMarker: zoom < 13 ? false : true
				})
			})
		})
		if (this.state.activeState == STATE_LIST.CHOOSE_FROM_MAP_STATE) {
			if (!this.state.isLoading) {
				switch (this.state.locationType) {
					case LOCATION_TYPE.PICK_UP:
						this.setState({
							pickupLatLong: region
						})
						break;
					case LOCATION_TYPE.DESTINATION:
						this.setState({
							destinationLatLong: region
						})
						break;
					default:
						break;
				}
				this.setState({
					isLoading: true
				})
				wait(500).then(() => {
					this.setState({
						isLoading: false
					})
				})
			}
		}
	}

	handleBackPress = () => {
		if (this.draggableView && this.state.drawerOpen) {
			this.draggableView.closeDrawer()
		}
	}

	toggleDrawerStatus = (drawerStatus) => {
		this.setState({ drawerOpen: drawerStatus })
	}

	focusLocationInput = () => {
		if (this.draggableView && this.locationTextInput) {
			this.draggableView.openDrawer()
			if (this.state.locationType == LOCATION_TYPE.PICK_UP && this.state.pickupLatLong != null) this.animateMapTo(this.state.pickupLatLong)
			if (this.state.locationType == LOCATION_TYPE.DESTINATION && this.state.destinationLatLong != null) this.animateMapTo(this.state.destinationLatLong)
			wait(500).then(() => {
				if (!this.state.drawerOpen) {
					Animated.timing(
						this.animatedDestinations,
						{
							toValue: 0,
							duration: 300,
							useNativeDriver: true
						}
					).start()
					this.locationTextInput.focus()
				}
			})
		}
	}

	inputFocused = () => {
		this.setState({ locationInputFocused: true })
	}

	chooseDestinationLocation = (chosenLocation) => () => {
		if (this.draggableView) {
			this.draggableView.closeDrawer()
		}
		this.setState({
			locationInput: chosenLocation.locationName,
			destinationName: chosenLocation.locationName,
			destinationLatLong: chosenLocation.locationLatLng,
			showLocationList: false
		})
		wait(500).then(this.handleConfirmMarker)
	}

	/**
	 * On Pick Up / Destination map icon clicked
	 */
	handleOpenMap = () => {
		if (this.draggableView) {
			this.draggableView.closeDrawer()
		}
		wait(1000).then(() => {
			if (this.state.locationType == LOCATION_TYPE.PICK_UP) {
				if (this.state.pickupLatLong) this.animateMapTo(this.state.pickupLatLong)
				else this.setState({ pickupLatLong: this.state.region })
			}
			else if (this.state.locationType == LOCATION_TYPE.DESTINATION) {
				if (this.state.destinationLatLong) this.animateMapTo(this.state.destinationLatLong)
				else this.setState({ destinationLatLong: this.state.region })
			}
			this.setState({
				activeState: STATE_LIST.CHOOSE_FROM_MAP_STATE
			})
		})
	}

	handleConfirmMarker = () => {
		this.setState({
			activeState: STATE_LIST.LOADING_STATE
		})
		wait(2000).then(() => {
			this.animateMapTo(this.state.pickupLatLong, this.state.destinationLatLong)
			this.setState({
				activeState: STATE_LIST.PRE_ORDER_STATE
			})
		})
	}

	changeLocation = (locationType) => () => {
		this.setState({
			activeState: STATE_LIST.CHOOSE_LOCATION_STATE,
			locationInput: locationType == LOCATION_TYPE.DESTINATION ? this.state.destinationName : this.state.pickupName,
			locationType: locationType,
			showLocationList: true
		}, this.focusLocationInput)
	}

	changePaymentMethod = (paymentMethod) => {
		this.setState({
			activePaymentMethod: paymentMethod
		})
		this.togglePaymentMethodModal()
	}

	changePromo = () => {
		if (this.state.activePromo) {
			this.setState({
				activePromo: ''
			})
		}
		else {
			this.setState({
				activePromo: '₦ 30 discount'
			})
		}
	}

	handleConfirmOrder = () => {
		this.setState({
			activeState: STATE_LIST.MATCHMAKING_STATE
		})
		wait(4000).then(() => {
			this.animateMapTo(this.state.pickupLatLong, this.state.vehicleMarker[0].latlng)
			this.setState({
				activeState: STATE_LIST.ORDER_PROGRESS_STATE
			})
		})
	}

	handleCancelMatchmaking = () => {
		this.animateMapTo(this.state.pickupLatLong, this.state.destinationLatLong)
		this.setState({
			activeState: STATE_LIST.PRE_ORDER_STATE
		})
	}

	togglePaymentMethodModal = () => this.setState({ paymentMethodModalVisible: !this.state.paymentMethodModalVisible })

	toggleCancelModal = () => this.setState({ cancelModalVisible: !this.state.cancelModalVisible })

	onSubmitCancelForm = (cancelReason) => {
		this.setState({
			activeState: STATE_LIST.PRE_ORDER_STATE
		})
		this.toggleCancelModal()
		wait(1000).then(() => {
			this.animateMapTo(this.state.pickupLatLong, this.state.destinationLatLong)
			if (this.draggableView) {
				this.draggableView.closeDrawer()
			}
			// alert(cancelReason)
		})
	}

	contactDriver = (contactType) => () => {
		if (contactType == 'call') call(this.state.driverNumber)
		else sms(this.state.driverNumber)
	}

	render() {
		const {
			drawerOpen,
			disableSwipe,
			activeState,
			serviceType,
			showMarker,

			// CHOOSE_LOCATION_STATE Variable
			searchLocationList,
			locationInputFocused,
			destinationName,
			destinationLatLong,
			locationInput,
			locationType,
			showLocationList,

			// CHOOSE_FROM_MAP_STATE Variable
			region,
			locationDescription,

			// PRE_ORDER_STATE Variable
			estimatePrice,
			estimateDistance,
			pickupName,
			pickupLatLong,
			activePromo,

			// ORDER_PROGRESS_STATE Variable
			currentOrderStatus,
			driverPhoto,
			driverName,
			driverPlate,
			driverNumber,

			// Cancel Modal Variable
			cancelOptionList,
			isLoadingCancel,
			cancelModalVisible,

			// Payment Method Modal Variable
			paymentMethodList,
			activePaymentMethod,
			paymentMethodModalVisible,


			initialLoad,
			initialRegion,
			isLoading,
			vehicleMarker
		} = this.state

		// List of Animated Variable

		const navbarTop = this.animatedMarker.interpolate({
			inputRange: [0, 1],
			outputRange: [-NAVBAR_HEIGHT, 0]
		})
		const markerSize = this.animatedMarker.interpolate({
			inputRange: [0, 1],
			outputRange: [0, 40]
		})
		const destinationHeaderMarginTop = this.animatedDestinations.interpolate({
			inputRange: [0, 1],
			outputRange: [-92, 0]
		})
		const destinationHeaderPaddingTop = this.animatedDestinations.interpolate({
			inputRange: [0, 1],
			outputRange: [92, 0]
		})
		const destinationTitleMarginTop = this.animatedDestinations.interpolate({
			inputRange: [0, 1],
			outputRange: [-40, 0]
		})
		const destinationTitleMarginBottom = this.animatedDestinations.interpolate({
			inputRange: [0, 1],
			outputRange: [20, 20]
		})

		// End of Animated Variable

		// =====================================================

		// Create The Payment Method Type Here

		let activePaymentMethodName = ''
		let activePaymentMethodImage = ''
		switch (activePaymentMethod) {
			case PAYMENT_TYPE.ALPHA_WALLET:
				activePaymentMethodName = 'Alpha Wallet'
				activePaymentMethodImage = Icons.iconWallet
				break;
			case PAYMENT_TYPE.CASH:
				activePaymentMethodName = 'Pay cash'
				activePaymentMethodImage = Icons.iconCash
				break;
			default:
				break;
		}

		// End of Create Payment Method Type

		// Create The Vehicle Type Here

		let vehicleMarkerImage = ''
		let vehicleServiceName = ''
		let vehicleServiceImage = ''

		switch (serviceType) {
			case SERVICE_TYPE.RIDE_BIKE:
				vehicleMarkerImage = Icons.iconMapMotorcycle
				vehicleServiceName = 'Ride Bike'
				vehicleServiceImage = Icons.iconMotorcycle
				break;
			case SERVICE_TYPE.RIDE_CAR:
				vehicleMarkerImage = Icons.iconMapCar
				vehicleServiceName = 'Ride Car'
				vehicleServiceImage = Icons.iconCar
				break;
			default:
				break;
		}

		// End of Create Vehicle Type

		// =====================================================

		// Create The Conditional View Here

		const loadingView = (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.WhiteColor }}>
				<ActivityIndicator color={Colors.PrimaryColor} />
			</View>
		)

		const inputDestinationView = (
			<View style={{
				flex: 1,
				backgroundColor: Colors.WhiteColor,
				paddingLeft: 18,
				paddingRight: 36,
				justifyContent: 'center',
				borderBottomWidth: 2,
				borderBottomColor: Colors.SemiGreyColor
			}}>
				<Animated.View
					style={{
						opacity: this.animatedDestinations,
						marginLeft: 8,
						marginTop: destinationTitleMarginTop,
						marginBottom: destinationTitleMarginBottom
					}}>
					<Text style={{ fontWeight: 'bold' }}>Where do you want to go ?</Text>
				</Animated.View>
				<View style={{ height: 32, width: '100%', flexDirection: 'row', alignItems: 'center' }}>
					{!drawerOpen && (
						<TouchableWithoutFeedback onPress={this.focusLocationInput}>
							<View style={{ height: '100%', width: '100%', position: 'absolute', zIndex: 1 }} />
						</TouchableWithoutFeedback>
					)}
					<View style={{ height: '100%', aspectRatio: 1, marginRight: 12, marginTop: 2 }}>
						<Image source={locationType == LOCATION_TYPE.DESTINATION ? Icons.iconDestinations : Icons.iconPickUp} resizeMode='contain' style={styles.staticImageSize} />
					</View>
					<View style={[{ flex: 1 }, !drawerOpen && { borderBottomWidth: 1, borderBottomColor: Colors.SemiGreyColor }]}>
						<TextInput
							ref={refs => { this.locationTextInput = refs }}
							onFocus={this.inputFocused}
							placeholder={locationType == LOCATION_TYPE.DESTINATION ? 'Input destination' : 'Input pickup location'}
							value={locationInput}
							onChangeText={this.onTextChange('locationInput')}
							style={{ flex: 1, padding: 0 }}
						/>
					</View>
					{drawerOpen && (
						<View style={{ flexDirection: 'row', marginRight: -4 }}>
							<View style={{ height: 25, width: 1, backgroundColor: Colors.SemiGreyColor, marginLeft: 16 }} />
							<TouchableOpacity onPress={this.handleOpenMap} style={{ paddingLeft: 16 }}>
								<View style={{ height: 25, aspectRatio: 1 }}>
									<Image source={Icons.iconMap} resizeMode='contain' style={styles.staticImageSize} />
								</View>
							</TouchableOpacity>
						</View>
					)}
				</View>
			</View >
		)

		const chooseFromMapView = (
			<View style={{
				flex: 1,
				backgroundColor: Colors.WhiteColor,
				paddingLeft: 32,
				paddingRight: 36,
				justifyContent: 'space-evenly'
			}}>
				<View style={{ height: 54, width: '100%', flexDirection: 'row' }}>
					<View style={{ height: 40, width: 32, marginRight: 12 }}>
						<Image source={locationType == LOCATION_TYPE.DESTINATION ? Icons.iconDestinations : Icons.iconPickUp} resizeMode='contain' style={styles.staticImageSize} />
					</View>
					<View style={{ flex: 1, paddingTop: 2, borderBottomWidth: 1, borderBottomColor: Colors.SemiGreyColor }}>
						<Text style={{ fontSize: 10, lineHeight: 16 }} numberOfLines={3}>{locationDescription ? locationDescription : 'Move the map, the address of the location will be shown in here'}</Text>
					</View>
				</View>
				<View style={{ paddingLeft: 4 }}>
					<CustomButton
						label='Confirm'
						isLoading={isLoading}
						onPress={this.handleConfirmMarker}
					/>
				</View>
			</View>
		)

		const preOrderView = (
			<View style={{
				flex: 1,
				backgroundColor: Colors.WhiteColor
			}}>
				<View style={{ height: 55, width: '100%', flexDirection: 'row', alignItems: 'center', paddingLeft: 18, paddingRight: 32 }}>
					<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
						<View style={{ height: 18, width: 26 }}>
							<Image style={styles.staticImageSize} resizeMode='contain' source={vehicleServiceImage} />
						</View>
					</View>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						<Text style={{ fontSize: 13, color: Colors.BlackFontColor, fontWeight: 'bold' }}>{vehicleServiceName}</Text>
					</View>
					<View style={{ flexDirection: 'row' }}>
						<Text style={{ fontSize: 10, marginRight: 2, color: Colors.GreyColor }}>{Currencies.Naira}</Text>
						<Text style={{ fontSize: 14, fontWeight: 'bold', color: Colors.BlackFontColor }}>{estimatePrice}</Text>
					</View>
				</View>

				<View style={{ width: '100%', paddingVertical: 5, paddingLeft: 18, paddingRight: 32, borderTopWidth: 1, borderTopColor: Colors.SemiGreyColor }}>
					<TouchableOpacity onPress={this.changeLocation(LOCATION_TYPE.PICK_UP)} style={{ height: 48, width: '100%', flexDirection: 'row' }}>
						<View style={{ height: '100%', width: 55, justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 32, width: 32 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconPickUp} />
							</View>
						</View>
						<View style={{ flex: 1, justifyContent: 'center' }}>
							<Text style={{ fontSize: 8, color: Colors.GreyColor, letterSpacing: 0.5 }}>PICKUP LOCATION</Text>
							<Text numberOfLines={1} style={{ fontSize: 12, fontWeight: 'bold', color: Colors.BlackFontColor }}>{pickupName}</Text>
						</View>
						<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
							<View style={{ height: 24, width: 24 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconArrow2} />
							</View>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={this.changeLocation(LOCATION_TYPE.DESTINATION)} style={{ height: 48, width: '100%', flexDirection: 'row' }}>
						<View style={{ height: '100%', width: 55, justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 32, width: 32 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconDestinations} />
							</View>
						</View>
						<View style={{ flex: 1, justifyContent: 'center' }}>
							<Text style={{ fontSize: 8, color: Colors.GreyColor, letterSpacing: 0.5 }}>DESTINATION LOCATION . {String(estimateDistance).replace('.', ',')} km</Text>
							<Text numberOfLines={1} style={{ fontSize: 12, fontWeight: 'bold', color: Colors.BlackFontColor }}>{destinationName}</Text>
						</View>
						<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
							<View style={{ height: 24, width: 24 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconArrow2} />
							</View>
						</View>
					</TouchableOpacity>
				</View>

				<View style={{ width: '100%', height: 7, backgroundColor: Colors.LightGreyColor, borderTopWidth: 0.5, borderTopColor: Colors.SemiGreyColor, borderBottomWidth: 0.5, borderBottomColor: Colors.SemiGreyColor }} />

				<View style={{ height: 55, width: '100%', flexDirection: 'row', alignItems: 'center', paddingLeft: 18, paddingRight: 32 }}>
					<TouchableOpacity onPress={this.changePaymentMethod} style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
						<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 22, width: 22 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={activePaymentMethodImage} />
							</View>
						</View>
						<Text style={{ fontSize: 12, fontWeight: 'bold' }}>{activePaymentMethodName}</Text>
					</TouchableOpacity>
					<View style={{ height: '50%', width: 1, backgroundColor: Colors.SemiGreyColor, marginLeft: 12 }} />
					<TouchableOpacity onPress={this.changePromo} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
						{activePromo ? (
							<View style={{ paddingVertical: 5, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: Colors.SemiGreyColor, borderRadius: 16 }}>
								<View style={{ height: 14, width: 14, marginRight: 4 }}>
									<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconApprove2} />
								</View>
								<Text style={{ fontSize: 10, color: Colors.BlackFontColor, letterSpacing: 0.4 }}>{activePromo}</Text>
							</View>
						) : (
								<Text style={{ fontSize: 12, color: Colors.BlackFontColor }}>Promo</Text>
							)}
					</TouchableOpacity>
				</View>

				<View style={{ paddingHorizontal: 32 }}>
					<CustomButton
						onPress={this.handleConfirmOrder}
						isLoading={isLoading}
						label='Book'
					/>
				</View>
			</View >
		)

		const matchmakingView = (
			<View style={{
				flex: 1,
				backgroundColor: Colors.WhiteColor,
				paddingVertical: 16,
				paddingHorizontal: 32,
				alignItems: 'center',
				justifyContent: 'space-around'
			}}>
				<View style={{ height: 24, width: 30 }}>
					<Image style={styles.staticImageSize} resizeMode='contain' source={vehicleServiceImage} />
				</View>
				<View style={{ width: '75%' }}>
					<CustomLoading />
				</View>
				<View style={{ alignItems: 'center', width: '75%', marginTop: 5, marginBottom: 15 }}>
					<Text style={{ fontSize: 10, color: Colors.BlackFontColor, letterSpacing: 0.4 }}>Searching Driver</Text>
				</View>
				<CustomButton
					onPress={this.handleCancelMatchmaking}
					outline
					label='Cancel'
				/>
			</View>
		)

		const orderProgressView = (
			<View style={{
				flex: 1,
			}}>
				<View style={{ width: '100%', height: 70, paddingVertical: 10, paddingHorizontal: 12 }}>
					<View style={[{ flex: 1, flexDirection: 'row', backgroundColor: Colors.WhiteColor, borderWidth: 1, borderColor: Colors.SemiGreyColor }, styles.containerLightShadow]}>
						<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 28, width: 28 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconNotif} />
							</View>
						</View>
						<View style={{ flex: 1, justifyContent: 'center', paddingRight: 22 }}>
							<Text style={{ fontSize: 12, color: Colors.GreyColor, letterSpacing: 0.5 }}>{currentOrderStatus}</Text>
						</View>
					</View>
				</View>
				<View style={{ flex: 1, backgroundColor: Colors.WhiteColor }}>
					<View style={{ height: 2, width: 30, backgroundColor: Colors.SemiGreyColor, alignSelf: 'center', marginTop: 8, marginBottom: 18 }} />
					<View style={{ height: 48, width: '100%', flexDirection: 'row', paddingHorizontal: 16, marginBottom: 15 }}>
						<View style={{ height: '100%', aspectRatio: 1, borderRadius: 24, overflow: 'hidden', marginRight: 12 }}>
							<Image style={styles.staticImageSize} resizeMode='contain' source={{ uri: driverPhoto }} />
						</View>
						<View style={{ flex: 1, paddingVertical: 2, justifyContent: 'space-between' }}>
							<Text numberOfLines={1} style={{ fontSize: 16, letterSpacing: 0.5, color: Colors.BlackFontColor }}>{driverName}</Text>
							<Text style={{ fontSize: 12, letterSpacing: 0.5, color: Colors.BlackFontColor }}>{driverPlate}</Text>
						</View>
					</View>
					<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', borderTopWidth: 1, borderTopColor: Colors.SemiGreyColor }}>
						<TouchableOpacity onPress={this.contactDriver('call')} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 25, width: 25, marginRight: 8 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconPhoneColor} />
							</View>
							<Text style={{ fontSize: 12, letterSpacing: 0.5, color: Colors.GreyColor }}>Call</Text>
						</TouchableOpacity>
						<View style={{ height: '50%', width: 1, backgroundColor: Colors.SemiGreyColor }} />
						<TouchableOpacity onPress={this.contactDriver('sms')} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
							<View style={{ height: 25, width: 25, marginRight: 8 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconChat} />
							</View>
							<Text style={{ fontSize: 12, letterSpacing: 0.5, color: Colors.GreyColor }}>SMS</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		)

		// End of Conditional View Creation

		// =====================================================

		// Setup Conditional View Here

		let navbarTitle = vehicleServiceName
		let activeDrawerHeight = CHOOSE_LOCATION_DRAWER_HEIGHT
		let activeDrawerView = inputDestinationView
		let activeMarker = <></>

		switch (activeState) {
			case STATE_LIST.LOADING_STATE:
				activeDrawerHeight = LOADING_DRAWER_HEIGHT
				activeDrawerView = loadingView
				activeMarker = (
					<>
						<Marker
							onSelect={null}
							coordinate={pickupLatLong}
						>
							<MarkerItem bigger imageSource={Icons.iconMapPickUp} />
						</Marker>
						{destinationLatLong != null && (
							<Marker
								onSelect={null}
								coordinate={destinationLatLong}
							>
								<MarkerItem bigger imageSource={Icons.iconMapPointerDestination} />
							</Marker>
						)}
					</>
				)
				break;
			case STATE_LIST.CHOOSE_LOCATION_STATE:
				if (locationInputFocused) {
					if (locationType == LOCATION_TYPE.DESTINATION) navbarTitle = 'Choose Destination'
					else navbarTitle = 'Choose Pickup Location'
				}
				activeDrawerHeight = CHOOSE_LOCATION_DRAWER_HEIGHT
				activeDrawerView = inputDestinationView
				activeMarker = (
					<>
						<Marker
							onSelect={null}
							coordinate={pickupLatLong}
						>
							<MarkerItem bigger imageSource={Icons.iconMapLocationMe} />
						</Marker>
						{vehicleMarker.map((marker, index) => showMarker && (
							<Marker
								onSelect={null}
								coordinate={marker.latlng}
								rotation={index * 45}
							>
								<MarkerItem imageSource={vehicleMarkerImage} />
							</Marker>
						))}
					</>
				)
				break;
			case STATE_LIST.CHOOSE_FROM_MAP_STATE:
				navbarTitle = 'Move the map to change location'
				activeDrawerHeight = CHOOSE_LOCATION_DRAWER_HEIGHT
				activeDrawerView = chooseFromMapView
				break;
			case STATE_LIST.PRE_ORDER_STATE:
				activeDrawerHeight = PRE_ORDER_DRAWER_HEIGHT
				activeDrawerView = preOrderView
				activeMarker = (
					<>
						<Marker
							onSelect={null}
							coordinate={pickupLatLong}
						>
							<MarkerItem bigger imageSource={Icons.iconMapPickUp} />
						</Marker>
						{destinationLatLong != null && (
							<Marker
								onSelect={null}
								coordinate={destinationLatLong}
							>
								<MarkerItem bigger imageSource={Icons.iconMapPointerDestination} />
							</Marker>
						)}
					</>
				)
				break;
			case STATE_LIST.MATCHMAKING_STATE:
				activeDrawerHeight = MATCHMAKING_DRAWER_HEIGHT
				activeDrawerView = matchmakingView
				activeMarker = (
					<>
						<Marker
							onSelect={null}
							coordinate={pickupLatLong}
						>
							<MarkerItem bigger imageSource={Icons.iconMapPickUp} />
						</Marker>
						{destinationLatLong != null && (
							<Marker
								onSelect={null}
								coordinate={destinationLatLong}
							>
								<MarkerItem bigger imageSource={Icons.iconMapPointerDestination} />
							</Marker>
						)}
					</>
				)
				break;
			case STATE_LIST.ORDER_PROGRESS_STATE:
				activeDrawerHeight = ORDER_PROGRESS_DRAWER_HEIGHT
				activeDrawerView = orderProgressView
				activeMarker = (
					<>
						<Marker
							onSelect={null}
							coordinate={pickupLatLong}
						>
							<MarkerItem bigger imageSource={Icons.iconMapPickUp} />
						</Marker>
						<Marker
							onSelect={null}
							coordinate={vehicleMarker[0].latlng}
						>
							<MarkerItem imageSource={vehicleMarkerImage} />
						</Marker>
					</>
				)
				break;
			default:
				break;
		}

		// End of Setup Conditional View

		// =====================================================

		const modalPaymentMethod = (
			<View style={{ paddingTop: 64, paddingBottom: 24, paddingLeft: 32, paddingRight: 40, backgroundColor: Colors.WhiteColor }}>
				<TouchableOpacity
					onPress={this.togglePaymentMethodModal}
					style={styles.closeContainer}>
					<Image source={Icons.iconDelete} style={{ flex: 1 }} resizeMode='contain' />
				</TouchableOpacity>
				<Text style={styles.modalText}>Choose payment method</Text>
				<FlatList
					data={paymentMethodList}
					extraData={paymentMethodList}
					keyExtractor={item => item}
					renderItem={({ item, index }) => <PaymentMethodItem item={item} index={index} changePaymentMethod={this.changePaymentMethod} />}
				/>
			</View>
		)

		const animatedNavbar = (
			<Animated.View style={[
				styles.headerContainer,
				!drawerOpen && styles.containerShadow,
				{ top: navbarTop }
			]}>
				<TouchableOpacity onPress={this.handleBackPress} style={styles.backContainer}>
					<View style={{ height: 24, width: 24 }}>
						<Image style={{ width: '100%', height: '100%' }} resizeMode='contain' source={Icons.iconLeftArrow} />
					</View>
				</TouchableOpacity>
				<View style={{ flex: 1, justifyContent: 'center' }}>
					<Text style={{ fontSize: activeState != STATE_LIST.CHOOSE_FROM_MAP_STATE ? 14 : 16, color: Colors.BlackColor }}>{navbarTitle}</Text>
					{/* <Animated.Text style={{ opacity: this.animatedDestinations, fontSize: 16, color: Colors.BlackColor }}>{navbarTitle}</Animated.Text> */}
				</View>
			</Animated.View>
		)

		const chooseFromMapMarkerPinPoint = (
			<View style={{ position: 'absolute', left: (width / 2) - 18, top: (height / 2) - 130 }}>
				<View style={{ width: 36, height: 72 }}>
					<Image source={locationType == LOCATION_TYPE.DESTINATION ? Icons.iconMapPointerDestination : Icons.iconMapPickUp} resizeMode='contain' style={styles.staticImageSize} />
				</View>
			</View>
		)

		const locationList = (
			<FlatList
				contentContainerStyle={{ paddingTop: 8 }}
				data={searchLocationList}
				extraData={searchLocationList}
				keyExtractor={item => item.locationName}
				renderItem={({ item, index }) => <LocationItem item={item} index={index} chooseDestinationLocation={this.chooseDestinationLocation} />}
			/>
		)

		const orderPreview = (
			<View style={{ height: 250 }}>
				<View style={{ width: '100%', paddingVertical: 5, paddingLeft: 18, paddingRight: 32, borderTopWidth: 1, borderTopColor: Colors.SemiGreyColor, borderBottomWidth: 1, borderBottomColor: Colors.SemiGreyColor }}>
					<View style={{ height: 48, width: '100%', flexDirection: 'row' }}>
						<View style={{ height: '100%', width: 55, justifyContent: 'center', alignItems: 'center', paddingTop: 3 }}>
							<View style={{ height: 32, width: 32 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconPickUp} />
							</View>
						</View>
						<View style={{ flex: 1, justifyContent: 'center' }}>
							<Text style={{ fontSize: 8, color: Colors.GreyColor, letterSpacing: 0.5 }}>PICKUP LOCATION</Text>
							<Text numberOfLines={1} style={{ fontSize: 12, fontWeight: 'bold', color: Colors.BlackFontColor }}>{pickupName}</Text>
						</View>
					</View>
					<View style={{ height: 48, width: '100%', flexDirection: 'row' }}>
						<View style={{ height: '100%', width: 55, justifyContent: 'center', alignItems: 'center', paddingTop: 5 }}>
							<View style={{ height: 32, width: 32 }}>
								<Image style={styles.staticImageSize} resizeMode='contain' source={Icons.iconDestinations} />
							</View>
						</View>
						<View style={{ flex: 1, justifyContent: 'center' }}>
							<Text style={{ fontSize: 8, color: Colors.GreyColor, letterSpacing: 0.5 }}>DESTINATION LOCATION . {String(estimateDistance).replace('.', ',')} km</Text>
							<Text numberOfLines={1} style={{ fontSize: 12, fontWeight: 'bold', color: Colors.BlackFontColor }}>{destinationName}</Text>
						</View>
					</View>
				</View>
				<View style={{ height: 55, width: '100%', flexDirection: 'row', alignItems: 'center', paddingLeft: 18, paddingRight: 32 }}>
					<View style={{ height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center' }}>
						<View style={{ height: 18, width: 26 }}>
							<Image style={styles.staticImageSize} resizeMode='contain' source={vehicleServiceImage} />
						</View>
					</View>
					<View style={{ flex: 1, justifyContent: 'center' }}>
						<Text style={{ fontSize: 13, color: Colors.BlackFontColor, fontWeight: 'bold' }}>{vehicleServiceName}</Text>
					</View>
					<View style={{ flexDirection: 'row' }}>
						<Text style={{ fontSize: 10, marginRight: 2, color: Colors.GreyColor }}>{Currencies.Naira}</Text>
						<Text style={{ fontSize: 14, fontWeight: 'bold', color: Colors.BlackFontColor }}>{estimatePrice}</Text>
					</View>
				</View>
				<View style={{ paddingVertical: 20, paddingHorizontal: 32 }}>
					<CustomButton
						onPress={this.toggleCancelModal}
						outline
						label='Cancel'
					/>
				</View>
			</View>
		)

		return (
			<View style={{ flex: 1 }}>
				<StatusBar backgroundColor={Colors.WhiteColor} barStyle='dark-content' />
				{/* NAVBAR */}
				{animatedNavbar}
				<DraggableView
					initialDrawerHeight={activeDrawerHeight}
					// height - 535 is equal to the number of space left before the max drawer
					// 535 is the desired drawer height
					finalDrawerHeight={activeState == STATE_LIST.ORDER_PROGRESS_STATE ? height - 535 : NAVBAR_HEIGHT - GOOGLE_LOGO_HEIGHT}
					refFunc={(c) => { this.draggableView = c }}
					// disableSwipe={disableSwipe}
					disableSwipe={disableSwipe || (activeState != STATE_LIST.CHOOSE_LOCATION_STATE && activeState != STATE_LIST.ORDER_PROGRESS_STATE)}
					onRelease={isDrawerOpen => {
						if (isDrawerOpen) {
							Animated.timing(
								this.animatedDestinations,
								{
									toValue: 0,
									duration: 300,
									// useNativeDriver: true
								}
							).start()
							if (activeState == STATE_LIST.CHOOSE_LOCATION_STATE) {
								wait(500).then(() => {
									this.locationTextInput.focus()
								})
							}
							this.toggleDrawerStatus(isDrawerOpen)
						}
						else {
							wait(500).then(() => {
								this.toggleDrawerStatus(isDrawerOpen)
							})
							Animated.timing(
								this.animatedDestinations,
								{
									toValue: 1,
									duration: 300,
									// useNativeDriver: true
								}
							).start()
							if (activeState == STATE_LIST.CHOOSE_LOCATION_STATE) {
								Keyboard.dismiss()
							}
						}

					}}
					renderContainerView={() => (
						<View style={[{ flex: 1, paddingBottom: 100 }, !drawerOpen && { zIndex: 1 }]}>
							{!initialLoad && (
								<MapView
									ref={mapView => { this.mapView = mapView; }}
									provider={PROVIDER_GOOGLE}
									onRegionChangeComplete={this.onRegionChange}
									showsUserLocation
									minZoomLevel={8}
									onTouchStart={() => {
										Animated.timing(
											this.animatedMarker,
											{
												toValue: 0,
												duration: 300
											}
										).start()
									}}
									onTouchEnd={() => {
										Animated.timing(
											this.animatedMarker,
											{
												toValue: 1,
												duration: 300
											}
										).start()
									}}
									style={{ flex: 1 }} initialRegion={initialRegion}>
									{activeMarker}
								</MapView>
							)}
							{/* Shows Map Marker to Pin Point Location */}
							{activeState == STATE_LIST.CHOOSE_FROM_MAP_STATE && chooseFromMapMarkerPinPoint}
							{/* Location debugger */}
							{activeState == STATE_LIST.CHOOSE_FROM_MAP_STATE && (
								<View style={styles.footer}>
									<Text style={styles.region}>{JSON.stringify(region, null, 4)}</Text>
								</View>
							)}
							<TouchableOpacity onPress={() => this.animateMapTo(initialRegion)} style={{ position: 'absolute', top: NAVBAR_HEIGHT + 20, right: 20 }}>
								<Animated.View style={{
									height: markerSize,
									width: markerSize,
									opacity: this.animatedMarker,
									justifyContent: 'center',
									alignItems: 'center',
									backgroundColor: Colors.WhiteColor,
									shadowColor: "#000",
									shadowOffset: {
										width: 0,
										height: 1,
									},
									shadowOpacity: 0.22,
									shadowRadius: 2.22,

									elevation: 3,
								}}>
									<View style={{ height: 20, aspectRatio: 1 }}>
										<Image source={Icons.iconPickLocation} resizeMode='contain' style={styles.staticImageSize} />
									</View>
								</Animated.View>
							</TouchableOpacity>
						</View>
					)}
					renderInitDrawerView={() => (
						<Animated.View
							style={[{
								height: activeDrawerHeight,
								width: '100%'
							},
							activeState == STATE_LIST.CHOOSE_LOCATION_STATE && {
								marginTop: destinationHeaderMarginTop,
								paddingTop: destinationHeaderPaddingTop
							}]}>
							<Animated.View style={[{ height: 30, width: 60, marginLeft: 24 }, activeState == STATE_LIST.CHOOSE_LOCATION_STATE && { opacity: this.animatedDestinations }, activeState == STATE_LIST.ORDER_PROGRESS_STATE && { marginBottom: -10 }]}>
								<Image source={Icons.iconGoogleLogo} resizeMode='contain' style={styles.staticImageSize} />
							</Animated.View>
							<View style={[{
								flex: 1,
								zIndex: 2
							}, !drawerOpen && styles.containerShadow]}>
								{activeDrawerView}
							</View>
						</Animated.View>
					)}
					renderDrawerView={() => (
						<View style={{ flex: 1, backgroundColor: activeState == STATE_LIST.CHOOSE_LOCATION_STATE ? Colors.LightGreyColor : Colors.WhiteColor }}>
							{activeState == STATE_LIST.CHOOSE_LOCATION_STATE && showLocationList && locationList}
							{activeState == STATE_LIST.ORDER_PROGRESS_STATE && orderPreview}
						</View>
					)}
				/>
				<Modal
					style={{ margin: 0, justifyContent: 'flex-end' }}
					children={modalPaymentMethod}
					isVisible={paymentMethodModalVisible}
					backdropOpacity={0.7}
				/>
				<CustomCancelModal
					cancelOptionList={cancelOptionList}
					isLoadingCancel={isLoadingCancel}
					cancelModalVisible={cancelModalVisible}
					onCancelPress={this.toggleCancelModal}
					onSubmitCancelForm={this.onSubmitCancelForm}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	headerContainer: {
		height: NAVBAR_HEIGHT,
		width: "100%",
		flexDirection: 'row',
		backgroundColor: Colors.WhiteColor,
		alignItems: 'center',
		position: 'absolute',
		paddingLeft: 4,
		zIndex: 2
	},
	backContainer: {
		height: 50,
		width: 50,
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: 6
	},
	map: {
		flex: 1
	},
	staticImageSize: {
		height: '100%',
		width: '100%'
	},
	containerLightShadow: {
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.20,
		shadowRadius: 1.41,

		elevation: 2,
	},
	containerShadow: {
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,

		elevation: 3,
	},
	markerFixed: {
		left: '50%',
		marginLeft: -24,
		marginTop: -48,
		position: 'absolute',
		top: '50%'
	},
	marker: {
		height: 48,
		width: 48
	},
	footer: {
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		bottom: 200,
		position: 'absolute',
		width: '100%',
		zIndex: 2
	},
	region: {
		color: '#fff',
		lineHeight: 20,
		marginHorizontal: 20,
		marginVertical: 5
	},
	closeContainer: {
		height: 25,
		width: 25,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		top: 25,
		left: 26,
		zIndex: 2
	},
	modalText: {
		fontSize: 18,
		fontWeight: 'bold',
		// fontFamily: Fonts.NunitoBold,
		lineHeight: 28,
		letterSpacing: 0.5,
		marginLeft: -1,
		marginBottom: 20
	}
})

export default DemoMapScreen;