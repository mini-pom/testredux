import { AppRegistry } from "react-native";
import App from "./src/App";
import { name as appName } from "./app.json";
import bgMessaging from './src/libraries/bgMessaging';

AppRegistry.registerComponent(appName, () => App);

// New task registration
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);