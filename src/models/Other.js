import fetchNoCache from '../libraries/fetchNoCache'

export const modelOther = {
    getBanner: (update) => {
        return fetchNoCache(update, APIGetBanner, 'GET')
    }
}